var cities = {
  "Zelina, Croatia": {
    "position": {
      "lat": 45.9554509, 
      "lng": 16.247051
    }, 
    "objects": [
      {
        "images": [
          "2006-recreation-hotel-health-complex-zelina.jpg"
        ], 
        "object": "2006 Recreation-Hotel-Health Complex, 50 .000 m2, under construction", 
        "area": 50000
      }
    ]
  }, 
  "Naantali, Finland": {
    "position": {
      "lat": 60.46608759999999, 
      "lng": 22.0250873
    }, 
    "objects": [
      {
        "images": [
          "1991-hotel-business-centre-naantali.jpg"
        ], 
        "object": "1991 Hotel-Business Centre, 58.000 m2, project", 
        "area": 58000
      }
    ]
  }, 
  "Ulaan Bataar, Mongolia": {
    "position": {
      "lat": 47.88639879999999, 
      "lng": 106.9057439
    }, 
    "objects": [
      {
        "images": [
          "1986-hotel-and-congress-centre-ulaan-bataar.jpg"
        ], 
        "object": "1986 Hotel and Congress Centre, 27.600 m2, constructed 1992, , 1st award at the internal competition", 
        "area": 27600
      }
    ]
  }, 
  "Dragomelj, Slovenia": {
    "position": {
      "lat": 46.1090576, 
      "lng": 14.5937865
    }, 
    "objects": [
      {
        "images": [
          "2006-apartment-complex-dragomelj.jpg"
        ], 
        "object": "2006 Apartment complex, 13.400 m2, 2st award at the competition", 
        "area": 13400
      }
    ]
  }, 
  "Roga\u0161ka Slatina, Slovenia": {
    "position": {
      "lat": 46.2331941, 
      "lng": 15.6378802
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2000 Shopping center Keros, 8.000 m2, constructed 2000", 
        "area": 8000
      }, 
      {
        "images": [], 
        "object": "2000 Shopping center AP, 6.000 m2, constructed 2000", 
        "area": 6000
      }, 
      {
        "images": [
          "2003-aparthotel-slatina-rogaka-slatina.jpg"
        ], 
        "object": "2003 Aparthotel Slatina, 4.100 m2, constructed 2003", 
        "area": 4100
      }, 
      {
        "images": [], 
        "object": "2004 Office and apartments building Hypoleasing, 1.600 m2, constructed 2004", 
        "area": 1600
      }, 
      {
        "images": [], 
        "object": "2004 Hotel Strossmayer, 3.700 m2, constructed 2004", 
        "area": 3700
      }, 
      {
        "images": [], 
        "object": "2004 Grand hotel, 7.900 m2, constructed 2004", 
        "area": 7900
      }, 
      {
        "images": [], 
        "object": "2004 Hotel Styria, 2.600 m2, constructed 2004", 
        "area": 2600
      }, 
      {
        "images": [], 
        "object": "2006 Hotel Soca, 6.000 m2, constructed 2008", 
        "area": 6000
      }, 
      {
        "images": [
          "2007-hotel-alfa-rogaka-slatina.jpg"
        ], 
        "object": "2007 Hotel Alfa, 22.000 m2, project", 
        "area": 22000
      }, 
      {
        "images": [
          "2008-police-station-rogaka-slatina.jpg"
        ], 
        "object": "2008 Police station, 3.300 m2, constructed 2008", 
        "area": 3300
      }, 
      {
        "images": [
          "2010-hotel-donat-rogaka-slatina.jpg"
        ], 
        "object": "2010 Hotel Donat, 19.000 m2, constructed 2010", 
        "area": 19000
      }, 
      {
        "images": [
          "2013-hotel-atlantida-rogaka-slatina-slovenia.jpg"
        ], 
        "object": "2013 Hotel Atlantida, 6.100 m2, under construction", 
        "area": 6100
      }, 
      {
        "images": [
          "2001-promenade-rogaka-slatina.jpg"
        ], 
        "object": "2001 Promenade", 
        "area": 0
      }, 
      {
        "images": [
          "2003-grand-hotel-zdravliki-dom-rogaka-slatina.jpg"
        ], 
        "object": "2003 Grand hotel \u00abZdravli\u0161ki dom\u00bb", 
        "area": 0
      }, 
      {
        "images": [
          "2004-apartment-building-rogaka-slatina.jpg"
        ], 
        "object": "2004 Apartment building", 
        "area": 0
      }, 
      {
        "images": [
          "2004-housek-rogaka-slatina.jpg"
        ], 
        "object": "2004 Housek", 
        "area": 0
      }, 
      {
        "images": [
          "2006-hotel-aleksander-rogaka-slatina.jpg"
        ], 
        "object": "2006 Hotel Aleksander", 
        "area": 0
      }, 
      {
        "images": [
          "2007-keiser-restaurant-rogaka-slatina.jpg"
        ], 
        "object": "2007 Keiser restaurant", 
        "area": 0
      }, 
      {
        "images": [
          "2010-cultural-centre-rogaka-slatina.jpg"
        ], 
        "object": "2010 Cultural centre", 
        "area": 0
      }, 
      {
        "images": [
          "2010-parking-garage-janina-rogaka-slatina.jpg"
        ], 
        "object": "2010 Parking garage \u00abJanina\u00bb", 
        "area": 0
      }, 
      {
        "images": [
          "2013-bus-station-and-parking-garage-rogaka-slatina.jpg"
        ], 
        "object": "2013 Bus station and parking garage, project", 
        "area": 0
      }
    ]
  }, 
  "Zapolyarny, Russia": {
    "position": {
      "lat": 69.4247316, 
      "lng": 30.8232995
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1994 Polyclinic, 6.200 m2, project", 
        "area": 6200
      }
    ]
  }, 
  "Samara, Russia": {
    "position": {
      "lat": 53.2415041, 
      "lng": 50.2212463
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Hotel-Congress Centre Mimel, 40.500 m2, project", 
        "area": 40500
      }, 
      {
        "images": [], 
        "object": "1995 Gynecology, Maternity and Pediatric Clinic, 26.000 m2, project", 
        "area": 26000
      }, 
      {
        "images": [
          "1996-business-centre-samara.jpg"
        ], 
        "object": "1996 Business Centre, 30.500 m2, project, 1st award at the internal competition", 
        "area": 30500
      }
    ]
  }, 
  "Cairo, Egypt": {
    "position": {
      "lat": 30.0444196, 
      "lng": 31.2357116
    }, 
    "objects": [
      {
        "images": [
          "1983-national-museum-of-egyptian-civilization-cairo.jpg"
        ], 
        "object": "1983 National Museum of Egyptian civilization, 21.000 m2, project 1983 Maternity Hospital, Ljubljana, Slovenia, 15.000 m2, project", 
        "area": 21000
      }, 
      {
        "images": [], 
        "object": "1985 Rayan Complex Bank, 9.500 m2, constructed 1987", 
        "area": 9500
      }, 
      {
        "images": [
          "1985-commercial-and-apartment-complex-pyramid-street-cairo.jpg"
        ], 
        "object": "1985 Commercial and Apartment Complex Pyramid Street, project", 
        "area": 0
      }, 
      {
        "images": [
          "1985-pyramid-gardens-apartment-complex-cairo.jpg"
        ], 
        "object": "1985 Pyramid Gardens Apartment complex, 5000 apartments, 140 ha, project, 1st award at the internal competition", 
        "area": 0
      }, 
      {
        "images": [
          "1986-rayan-headquarter-gize.jpg"
        ], 
        "object": "1986 Rayan Headquarter, Gize, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [
          "1986-department-store-rayan.jpg"
        ], 
        "object": "1986 Department Store, Rayan, 4.000 m2, project", 
        "area": 4000
      }, 
      {
        "images": [
          "1999-business-centre-and-parking-houses-cairo.jpg"
        ], 
        "object": "1999 Business Centre and Parking Houses, 30.000 m2, project 2002", 
        "area": 30000
      }
    ]
  }, 
  "Slovenia": {
    "position": {
      "lat": 46.151241, 
      "lng": 14.995463
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2006 Eurospin shops, 4.500 m2, constructed 2006 and 2007", 
        "area": 4500
      }
    ]
  }, 
  "Ivangrad, Montenegro": {
    "position": {
      "lat": 42.83793439999999, 
      "lng": 19.8603732
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1981 General Hospital, 12.000 m2, constructed 1983", 
        "area": 12000
      }
    ]
  }, 
  "Beograd, Serbia": {
    "position": {
      "lat": 44.786568, 
      "lng": 20.4489216
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1985 Headquarters JAT-Jugopetrol, 24.000 m2, project", 
        "area": 24000
      }, 
      {
        "images": [
          "1989-hotel-jat-beograd.jpg"
        ], 
        "object": "1989 Hotel Jat, 10.000 m2, project", 
        "area": 10000
      }, 
      {
        "images": [
          "1989-headquarter-dom-beograd.jpg"
        ], 
        "object": "1989 Headquarter DOM, 8.000 m2, project", 
        "area": 8000
      }
    ]
  }, 
  "Chelyabinsk, Russia": {
    "position": {
      "lat": 55.1644419, 
      "lng": 61.4368431
    }, 
    "objects": [
      {
        "images": [
          "1997-business-and-hotel-amusement-centre-chelyabinsk.jpg"
        ], 
        "object": "1997 Business and Hotel-Amusement Centre, 53.000 m2, project, 1st award at the internal competition", 
        "area": 53000
      }, 
      {
        "images": [], 
        "object": "2002 Aqua Centre, 27.000 m2, project", 
        "area": 27000
      }
    ]
  }, 
  "Aleksandrovsk, Russia": {
    "position": {
      "lat": 59.15623280000001, 
      "lng": 57.5942158
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1992 General Hospital, 24.000 m2, project", 
        "area": 24000
      }
    ]
  }, 
  "Belgorod, Russia": {
    "position": {
      "lat": 50.5997134, 
      "lng": 36.5982621
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2003 Poultry Farm, 156.000 m2, constructed 2006", 
        "area": 156000
      }
    ]
  }, 
  "Maldives": {
    "position": {
      "lat": 1.977247, 
      "lng": 73.5361034
    }, 
    "objects": [
      {
        "images": [
          "1987-tourist-centre-maldives.jpg"
        ], 
        "object": "1987 Tourist Centre, 4.000 m2, project", 
        "area": 4000
      }
    ]
  }, 
  "Sezana, Slovenia": {
    "position": {
      "lat": 45.7093519, 
      "lng": 13.8738185
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2004 Entertainment complex, 200.000 m2, project", 
        "area": 200000
      }
    ]
  }, 
  "Tanzania": {
    "position": {
      "lat": -6.369028, 
      "lng": 34.888822
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1989 Hotel DarEs Salaam, 13.000 m2, project", 
        "area": 13000
      }
    ]
  }, 
  "Golnik, Slovenia": {
    "position": {
      "lat": 46.3269968, 
      "lng": 14.3332684
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2001 Pulmonology Clinic, 1.500 m2, constructed 2001", 
        "area": 1500
      }
    ]
  }, 
  "Sochi, Russia": {
    "position": {
      "lat": 43.60280789999999, 
      "lng": 39.7341543
    }, 
    "objects": [
      {
        "images": [
          "1999-hotel-complex-brincalov-sochi.jpg"
        ], 
        "object": "1999 Hotel Complex Brincalov, 30.000 m2, project", 
        "area": 30000
      }, 
      {
        "images": [
          "2010-hotel-and-health-resort-reconstruction-sochi.jpg"
        ], 
        "object": "2010 Hotel and Health Resort, reconstruction, 11.200 m2, constructed 2013", 
        "area": 11200
      }
    ]
  }, 
  "Nova Gorica, Slovenia": {
    "position": {
      "lat": 45.9549755, 
      "lng": 13.6493044
    }, 
    "objects": [
      {
        "images": [
          "1970-general-hospital-nova-gorica.jpg"
        ], 
        "object": "1970 General Hospital, 33.000 m2, constructed 1981, newspaper \u00abBORBA\u00bb award 1975", 
        "area": 33000
      }, 
      {
        "images": [
          "1993-perla-hotel-and-casino-nova-gorica.jpg"
        ], 
        "object": "1993 Perla Hotel and Casino, 16.000 m2, constructed 1994", 
        "area": 16000
      }, 
      {
        "images": [], 
        "object": "1993 Business Centre, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [], 
        "object": "1997 Hotel Park, 3.000 m2, constructed 1997", 
        "area": 3000
      }, 
      {
        "images": [
          "1998-hotel-and-casino-perla-2nd-stage-nova-gorica.jpg"
        ], 
        "object": "1998 Hotel and Casino Perla \u2013 2nd stage, 36.000 m2, constructed 2002", 
        "area": 36000
      }
    ]
  }, 
  "Ashgabad, Turkmenistan": {
    "position": {
      "lat": 37.9600766, 
      "lng": 58.32606289999999
    }, 
    "objects": [
      {
        "images": [
          "1998-olympic-complex-ashgabad.jpg"
        ], 
        "object": "1998 Olympic Complex, 247.000 m2, project", 
        "area": 247000
      }
    ]
  }, 
  "Sberbank Rostov, Russia": {
    "position": {
      "lat": 47.214668, 
      "lng": 39.685188
    }, 
    "objects": [
      {
        "images": [
          "2002-business-centre-sberbank-rostov.jpg"
        ], 
        "object": "2002 Business Centre, 29.000 m2, constructed 2006, 1st award at the internal competition", 
        "area": 29000
      }
    ]
  }, 
  "Kiev, Ukraine": {
    "position": {
      "lat": 50.4501, 
      "lng": 30.5234
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Business Centre and Polyclinic, 79.000 m2, project", 
        "area": 79000
      }
    ]
  }, 
  "Arhangelsk, Russia": {
    "position": {
      "lat": 64.54725069999999, 
      "lng": 40.5601553
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1989 Polyclinic, 8.200 m2, project", 
        "area": 8200
      }
    ]
  }, 
  "Ptuj, Slovenia": {
    "position": {
      "lat": 46.4199535, 
      "lng": 15.8696884
    }, 
    "objects": [
      {
        "images": [
          "1990-maternity-hospital-ptuj.jpg"
        ], 
        "object": "1990 Maternity Hospital, 3.000 m2, constructed 1991", 
        "area": 3000
      }, 
      {
        "images": [], 
        "object": "2010 Shopping center, 23.000 m2, constructed 2010", 
        "area": 23000
      }
    ]
  }, 
  "Murmansk, Russia": {
    "position": {
      "lat": 68.9585244, 
      "lng": 33.0826598
    }, 
    "objects": [
      {
        "images": [
          "2002-shopping-and-recreation-centre-murmansk.jpg"
        ], 
        "object": "2002 Shopping and Recreation Centre, 21.000 m2, project", 
        "area": 21000
      }
    ]
  }, 
  "Zagreb, Croatia": {
    "position": {
      "lat": 45.8150108, 
      "lng": 15.981919
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1975 New Military Hospital, 60.500 m2, constructed 1989, 1st award at the YU competition", 
        "area": 60500
      }, 
      {
        "images": [
          "1984-university-hospital-zagreb.jpg"
        ], 
        "object": "1984 University Hospital, 170.000 m2, under construction, 1st award at the YU competition", 
        "area": 170000
      }, 
      {
        "images": [
          "1991-zagreb-city-development-study.jpg"
        ], 
        "object": "1991 Zagreb City, development study, project", 
        "area": 0
      }, 
      {
        "images": [
          "1991-exhibition-hotel-business-complex-velesajam-zagreb.jpg"
        ], 
        "object": "1991 Exhibition Hotel-Business Complex Velesajam, 280.000 m2, project", 
        "area": 280000
      }, 
      {
        "images": [], 
        "object": "2004 Lesnina centre, 32.000 m2, constructed 2004", 
        "area": 32000
      }
    ]
  }, 
  "Astana, Kazakhstan": {
    "position": {
      "lat": 51.16052269999999, 
      "lng": 71.4703558
    }, 
    "objects": [
      {
        "images": [
          "1996-parliament-of-the-republic-of-kazakhstan-astana.jpg"
        ], 
        "object": "1996 Parliament of the Republic of Kazakhstan, 16.000 m2, constructed 1997", 
        "area": 16000
      }, 
      {
        "images": [
          "1996-parliaments-residence-astana.jpg"
        ], 
        "object": "1996 Parliament's Residence, 15.600 m2, constructed 1997", 
        "area": 15600
      }, 
      {
        "images": [
          "1996-parliament-administration-building-astana.jpg"
        ], 
        "object": "1996 Parliament Administration Building, 27.800 m2, constructed 1997", 
        "area": 27800
      }, 
      {
        "images": [
          "1996-residence-of-the-president.jpg"
        ], 
        "object": "1996 Residence of the President of the R. Kazakhstan, 38.000 m2, project", 
        "area": 38000
      }, 
      {
        "images": [
          "1997-airport-astana.jpg"
        ], 
        "object": "1997 Airport, 31.000 m2, project", 
        "area": 31000
      }, 
      {
        "images": [
          "1997-hotel-ishin-astana.jpg"
        ], 
        "object": "1997 Hotel Ishin, 16.000 m2, project", 
        "area": 16000
      }, 
      {
        "images": [], 
        "object": "1998 Headquarter Kazakhoil, 42.600 m2, project", 
        "area": 42600
      }, 
      {
        "images": [
          "2000-city-palace-astana.jpg"
        ], 
        "object": "2000 City Palace, 59.000 m2, project", 
        "area": 59000
      }, 
      {
        "images": [], 
        "object": "2000 Transport Tower-Business Complex, 35.000 m2, project", 
        "area": 35000
      }, 
      {
        "images": [], 
        "object": "2001 Business Centre and Apartments, 42.500 m2, project", 
        "area": 42500
      }, 
      {
        "images": [
          "2004-pediatric-and-maternity-hospital-astana.jpg"
        ], 
        "object": "2004 Pediatric and Maternity Hospital, 53.000 m2, constructed 2007", 
        "area": 53000
      }, 
      {
        "images": [
          "2004-residence-complex-saranda-astana.jpg"
        ], 
        "object": "2004 Residence Complex Saranda, 201.000 m2, under construction", 
        "area": 201000
      }, 
      {
        "images": [
          "2004-diagnostic-vip-centre-and-hotel-astana.jpg"
        ], 
        "object": "2004 Diagnostic VIP Centre and Hotel, 27.000 m2, project", 
        "area": 27000
      }, 
      {
        "images": [
          "2004-sport-palace-velodrom-astana.jpg"
        ], 
        "object": "2004 Sport Palace Velodrom, 46.800 m2, constructed 2011", 
        "area": 46800
      }, 
      {
        "images": [], 
        "object": "2005 Oncology Clinic, 31.000 m2, project", 
        "area": 31000
      }, 
      {
        "images": [], 
        "object": "2005 Emergency Centre, 7 .000 m2, project", 
        "area": 7000
      }, 
      {
        "images": [], 
        "object": "2006 Diagnostic Centre, 14.200 m2, constructed 2010", 
        "area": 14200
      }, 
      {
        "images": [
          "2012-business-centre-sine-tempore-astana.jpg"
        ], 
        "object": "2012 Business Centre Sine Tempore, 40.000 m2, project", 
        "area": 40000
      }, 
      {
        "images": [], 
        "object": "2012 Business Centre Sine Tempore, 40.000 m2, project", 
        "area": 40000
      }, 
      {
        "images": [
          "2013-sport-complex-astana.jpg"
        ], 
        "object": "2013 Sport complex, 83.300 m2, under construction", 
        "area": 83300
      }, 
      {
        "images": [
          "2013-business-centre-mabatex-astana.jpg"
        ], 
        "object": "2013 Business Centre Mabatex", 
        "area": 0
      }
    ]
  }, 
  "Bashkortostan, Russia": {
    "position": {
      "lat": 54.2312172, 
      "lng": 56.1645257
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1996 Airport, UFA, 13.000 m2, project", 
        "area": 13000
      }
    ]
  }, 
  "Rostov, Russia": {
    "position": {
      "lat": 47.2357137, 
      "lng": 39.701505
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1996 Customhouse, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "Kirovakan, Armenia": {
    "position": {
      "lat": 40.807399, 
      "lng": 44.4970268
    }, 
    "objects": [
      {
        "images": [
          "1989-general-hospital-kirovakan.jpg"
        ], 
        "object": "1989 General Hospital, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "Split, Croatia": {
    "position": {
      "lat": 43.5081323, 
      "lng": 16.4401935
    }, 
    "objects": [
      {
        "images": [
          "1972-sport-hall-split.jpg"
        ], 
        "object": "1972 Sport Hall, 10.000 m2, project", 
        "area": 10000
      }, 
      {
        "images": [], 
        "object": "2005 Lesnina centre, 23.600 m2, constructed 2005", 
        "area": 23600
      }
    ]
  }, 
  "Slovenska Bistrica, Slovenia": {
    "position": {
      "lat": 46.3919813, 
      "lng": 15.5727868
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2007 Residential Complex, 13.600 m2, under construction", 
        "area": 13600
      }
    ]
  }, 
  "Vrbas, Serbia": {
    "position": {
      "lat": 45.5701534, 
      "lng": 19.6449683
    }, 
    "objects": [
      {
        "images": [
          "1979-medical-centre-vrbas.jpg"
        ], 
        "object": "1979 Medical Centre, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "N\u00fcrnberg, Germany": {
    "position": {
      "lat": 49.45203, 
      "lng": 11.07675
    }, 
    "objects": [
      {
        "images": [
          "1990-national-museum-nrnberg.jpg"
        ], 
        "object": "1990 National Museum, 14.000 m2, project", 
        "area": 14000
      }
    ]
  }, 
  "Izola, Slovenia": {
    "position": {
      "lat": 45.5374048, 
      "lng": 13.6600802
    }, 
    "objects": [
      {
        "images": [
          "2006-hotel-arigoni-izola.jpg"
        ], 
        "object": "2006 Hotel Arigoni, 36.200 m2, project", 
        "area": 36200
      }
    ]
  }, 
  "Magnitogorsk, Russia": {
    "position": {
      "lat": 53.4129429, 
      "lng": 59.00162330000001
    }, 
    "objects": [
      {
        "images": [
          "1988-general-hospital-magnitogorsk.jpg"
        ], 
        "object": "1988 General Hospital, 55.000 m2, project", 
        "area": 55000
      }, 
      {
        "images": [], 
        "object": "1993 Hotel, 1.700 m2, project", 
        "area": 1700
      }
    ]
  }, 
  "Tobolsk, Russia": {
    "position": {
      "lat": 58.200024, 
      "lng": 68.26352279999999
    }, 
    "objects": [
      {
        "images": [
          "1993-hotel-tobolsk.jpg"
        ], 
        "object": "1993 Hotel, 11.600 m2, constructed 1997", 
        "area": 11600
      }
    ]
  }, 
  "Mariupole, Ukraine": {
    "position": {
      "lat": 47.097133, 
      "lng": 37.543367
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1990 Polyclinic, 18.000 m2, project", 
        "area": 18000
      }
    ]
  }, 
  "Budva, Montenegro": {
    "position": {
      "lat": 42.2911489, 
      "lng": 18.840295
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1987 Hotel Complex Jaz, 40.000 m2, project", 
        "area": 40000
      }, 
      {
        "images": [], 
        "object": "1987 Hotel Complex Jaz, 40 .000 m2, project", 
        "area": 40000
      }
    ]
  }, 
  "Tunis, Tunisia": {
    "position": {
      "lat": 36.8064948, 
      "lng": 10.1815316
    }, 
    "objects": [
      {
        "images": [
          "1968-radio-tv-centre-tunis.jpg"
        ], 
        "object": "1968 Radio TV Centre, 22.000 m2, project", 
        "area": 22000
      }, 
      {
        "images": [
          "1983-arabic-league-headquarter-tunis.jpg"
        ], 
        "object": "1983 Arabic League Headquarter, 28.000 m2, project", 
        "area": 28000
      }
    ]
  }, 
  "Havana, Cuba": {
    "position": {
      "lat": 23.1135925, 
      "lng": -82.3665956
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1986 Hotels Riviera and Cuba Libre, 12.000 m2, project", 
        "area": 12000
      }, 
      {
        "images": [], 
        "object": "1991 Gynecology-Maternity Clinic, 132.000 m2, project", 
        "area": 132000
      }
    ]
  }, 
  "Pristina, Kosovo": {
    "position": {
      "lat": 42.6629138, 
      "lng": 21.1655028
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2006 General Hospital, 15.200 m2, project", 
        "area": 15200
      }
    ]
  }, 
  "Riga, Latvia": {
    "position": {
      "lat": 56.9496487, 
      "lng": 24.1051864
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Bonus Bank, 1.000 m2, constructed 1994", 
        "area": 1000
      }
    ]
  }, 
  "Yaroslavl, Russia": {
    "position": {
      "lat": 57.62607440000001, 
      "lng": 39.8844708
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2003 Business Centre, Sberbank, 13.000 m2, constructed 2009", 
        "area": 13000
      }, 
      {
        "images": [
          "2006-hotel-and-business-centre-yaroslavl.jpg"
        ], 
        "object": "2006 Hotel and Business Centre, 14.300 m2, project", 
        "area": 14300
      }, 
      {
        "images": [
          "2005-business-centre-yaroslavl.jpg"
        ], 
        "object": "2005 Business Centre", 
        "area": 0
      }
    ]
  }, 
  "Milan, Italy": {
    "position": {
      "lat": 45.4654219, 
      "lng": 9.1859243
    }, 
    "objects": [
      {
        "images": [
          "1991-general-hospital-milan.jpg"
        ], 
        "object": "1991 General Hospital, 40.000 m2, constructed 1996", 
        "area": 40000
      }
    ]
  }, 
  "Puerto Rico, USA": {
    "position": {
      "lat": 26.649503, 
      "lng": -98.3455688
    }, 
    "objects": [
      {
        "images": [
          "1995-tourist-complex-puerto-rico.jpg"
        ], 
        "object": "1995 Tourist Complex, 153.000 m2, project", 
        "area": 153000
      }
    ]
  }, 
  "Kruje, Albania": {
    "position": {
      "lat": 41.5094765, 
      "lng": 19.7710732
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1998 General Hospital, 6.000 m2, project", 
        "area": 6000
      }
    ]
  }, 
  "Gubkin, Russia": {
    "position": {
      "lat": 51.2824173, 
      "lng": 37.543495
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Hotel, 3.200 m2, project", 
        "area": 3200
      }
    ]
  }, 
  "Novosibirsk, Russia": {
    "position": {
      "lat": 55.00835259999999, 
      "lng": 82.9357327
    }, 
    "objects": [
      {
        "images": [
          "2008-sports-entertainment-complex-novosibirsk.jpg"
        ], 
        "object": "2008 Sports Entertainment Complex, 25.000 m2, under construction", 
        "area": 25000
      }, 
      {
        "images": [
          "2008-hotel-and-recreation-complex-obsko-more-novosibirsk.jpg"
        ], 
        "object": "2008 Hotel and Recreation Complex \u00abObsko more\u00bb, 145.000 m2, project", 
        "area": 145000
      }, 
      {
        "images": [], 
        "object": "2008 Business Complex \u00abSeverna vorota\u00bb, 300.000 m2, project", 
        "area": 300000
      }, 
      {
        "images": [], 
        "object": "2008 Recreation Complex \u00abPlaza\u00bb, 70.000 m2, project", 
        "area": 70000
      }, 
      {
        "images": [
          "2011-youth-entertainment-club-novosibirsk.jpg"
        ], 
        "object": "2011 Youth Entertainment Club, 9 .500 m2, project", 
        "area": 9500
      }
    ]
  }, 
  "Kamnik, Slovenia": {
    "position": {
      "lat": 46.2221964, 
      "lng": 14.6072968
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1976 Gymnasium, 8.000 m2, project", 
        "area": 8000
      }, 
      {
        "images": [], 
        "object": "1978 Kindergarten, 2 .500 m2, constructed 1981", 
        "area": 2500
      }
    ]
  }, 
  "Koper, Slovenia": {
    "position": {
      "lat": 45.54805899999999, 
      "lng": 13.7301877
    }, 
    "objects": [
      {
        "images": [
          "2007-apartment-building-koper.jpg"
        ], 
        "object": "2007 Apartment building, 6.300 m2, constructed", 
        "area": 6300
      }, 
      {
        "images": [
          "2011-apartment-building-koper.jpg"
        ], 
        "object": "2011 Apartment building", 
        "area": 0
      }
    ]
  }, 
  "Ticino, Switzerland": {
    "position": {
      "lat": 46.331734, 
      "lng": 8.800452900000002
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1995 Hotel, Health and Congress Centre, Aqua Rossa, 19.500 m2, project", 
        "area": 19500
      }
    ]
  }, 
  "Nizhny Tagil, Russia": {
    "position": {
      "lat": 57.9214912, 
      "lng": 59.9816186
    }, 
    "objects": [
      {
        "images": [
          "1992-hotel-nizhny-tagil.jpg"
        ], 
        "object": "1992 Hotel, 5.000 m2, project", 
        "area": 5000
      }, 
      {
        "images": [], 
        "object": "1992 Pediatric Hospital, 55.000 m2, project", 
        "area": 55000
      }
    ]
  }, 
  "Kuendu, New Caledonia": {
    "position": {
      "lat": -22.2583752, 
      "lng": 166.390135
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1992 Tourist Complex, 45.000 m2, project", 
        "area": 45000
      }
    ]
  }, 
  "Skopje, Macedonia": {
    "position": {
      "lat": 41.9973462, 
      "lng": 21.4279956
    }, 
    "objects": [
      {
        "images": [
          "1968-cultural-centre-skopje-1.jpg", 
          "1968-cultural-centre-skopje-2.jpg"
        ], 
        "object": "1968 Cultural Centre, 62.000 m2, constructed 1981, 1st award at the YU competition", 
        "area": 62000
      }, 
      {
        "images": [], 
        "object": "1969 Youth Academy Centre, 45.000 m2, project", 
        "area": 45000
      }
    ]
  }, 
  "Volzhskiy, Russia": {
    "position": {
      "lat": 48.8176494, 
      "lng": 44.7707294
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 School and Sports Centre, 18.050 m2, project", 
        "area": 18050
      }
    ]
  }, 
  "Tirana, Albania": {
    "position": {
      "lat": 41.3275459, 
      "lng": 19.8186982
    }, 
    "objects": [
      {
        "images": [
          "1998-general-hospital-tirana.jpg"
        ], 
        "object": "1998 General Hospital, 16.000 m2, project", 
        "area": 16000
      }
    ]
  }, 
  "Zaisan, Kazakhstan": {
    "position": {
      "lat": 47.4701444, 
      "lng": 84.87529959999999
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1992 Sanatorium Arsan Taldi, 6.000 m2, project", 
        "area": 6000
      }
    ]
  }, 
  "Sarajevo, Bosnia and Herzegovina": {
    "position": {
      "lat": 43.8562586, 
      "lng": 18.4130763
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1996 Hotel Bristol, 21.000 m2, project", 
        "area": 21000
      }, 
      {
        "images": [
          "2007-residential-complex-sarajevo.jpg"
        ], 
        "object": "2007 Residential Complex, 32.000 m2, project", 
        "area": 32000
      }
    ]
  }, 
  "Dushanbe, Tadjikistan": {
    "position": {
      "lat": 38.5597722, 
      "lng": 68.7870384
    }, 
    "objects": [
      {
        "images": [
          "2003-headquarter-orienbank-dushanbe.jpg"
        ], 
        "object": "2003 Headquarter Orienbank, 9.600 m2, constructed 2009", 
        "area": 9600
      }, 
      {
        "images": [
          "2012-sport-centre-dushanbe-tadjikistan.jpg"
        ], 
        "object": "2012 Sport Centre, 34.600 m2, project, 1st award at the internal competition", 
        "area": 34600
      }
    ]
  }, 
  "Dudinka, Russia": {
    "position": {
      "lat": 69.40419539999999, 
      "lng": 86.20029099999999
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2002 Maternity Hospital, 4.700 m2, constructed 2004", 
        "area": 4700
      }, 
      {
        "images": [], 
        "object": "2002 Regional Hospital, 9.500 m2, constructed 2005", 
        "area": 9500
      }, 
      {
        "images": [
          "2003-fitness-centre-dudinka-russia.jpg"
        ], 
        "object": "2003 Fitness Centre, 8.000 m2, project", 
        "area": 8000
      }
    ]
  }, 
  "Russia": {
    "position": {
      "lat": 61.52401, 
      "lng": 105.318756
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1990 Hotel Zukovo, 7.000 m2, project", 
        "area": 7000
      }, 
      {
        "images": [], 
        "object": "2000 Anti-Narcotics Centers, typology, 7.000 m2 \u2013 15.000 m2, project", 
        "area": 7000
      }
    ]
  }, 
  "Bled, Slovenia": {
    "position": {
      "lat": 46.3683266, 
      "lng": 14.1145798
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2003 Grand hotel Toplice, 5.200 m2, constructed 2003", 
        "area": 5200
      }, 
      {
        "images": [], 
        "object": "2008 Apartment Settlement, 15.000 m2, project", 
        "area": 15000
      }
    ]
  }, 
  "Astrakhan, Russia": {
    "position": {
      "lat": 46.3588045, 
      "lng": 48.0599345
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1992 Residential Complex, 30.000 m2, project", 
        "area": 30000
      }, 
      {
        "images": [
          "1994-agroprombank-astrakhan.jpg"
        ], 
        "object": "1994 Agroprombank, 8.000 m2, constructed 1998 1994 Hotel Druzba, Mariupole, Ukraine, 5.800 m2, constructed 1998", 
        "area": 8000
      }, 
      {
        "images": [], 
        "object": "2003 Airport building, 13.940 m2, project", 
        "area": 13940
      }, 
      {
        "images": [
          "2003-sport-centre-astrakhan.jpg"
        ], 
        "object": "2003 Sport Centre, 14.100 m2, project, 1st award at the internal competition", 
        "area": 14100
      }
    ]
  }, 
  "Maribor, Slovenia": {
    "position": {
      "lat": 46.5546503, 
      "lng": 15.6458812
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1981 Medical-Diagnostic Centre, 18.000 m2, constructed 1985, 1st award at the internal competition", 
        "area": 18000
      }, 
      {
        "images": [
          "1984-pediatric-hospital-maribor.jpg"
        ], 
        "object": "1984 Pediatric Hospital, 5.000 m2, constructed 1986, 1st award at the internal competition", 
        "area": 5000
      }, 
      {
        "images": [
          "1996-business-complex-maribor-city-maribor.jpg"
        ], 
        "object": "1996 Business Complex Maribor City, project", 
        "area": 0
      }, 
      {
        "images": [
          "2000-indoor-swimming-pool-pristan.jpg"
        ], 
        "object": "2000 Indoor Swimming Pool, Pristan, 12.000 m2, constructed 2002", 
        "area": 12000
      }, 
      {
        "images": [], 
        "object": "2002 Psychiatric Clinic, 15.500 m2, project", 
        "area": 15500
      }, 
      {
        "images": [], 
        "object": "2003 Aparthotel Zeleznicarski dom, 4.000 m2, constructed 2003", 
        "area": 4000
      }
    ]
  }, 
  "Murska Sobota, Slovenia": {
    "position": {
      "lat": 46.6581381, 
      "lng": 16.1610293
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2007 Diana Hotel, 10.500 m2, constructed 2010", 
        "area": 10500
      }
    ]
  }, 
  "Meljine, Montenegro": {
    "position": {
      "lat": 42.4560981, 
      "lng": 18.5596741
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1986 Military Hospital, 30.000 m2, project, 1st award at the YU competition", 
        "area": 30000
      }
    ]
  }, 
  "Kathmandu, Nepal": {
    "position": {
      "lat": 27.7172453, 
      "lng": 85.3239605
    }, 
    "objects": [
      {
        "images": [
          "1984-nepal-national-parliament-building-kathmandu.jpg"
        ], 
        "object": "1984 Nepal National Parliament Building, 7.500 m2, project", 
        "area": 7500
      }
    ]
  }, 
  "Sinai, Egypt": {
    "position": {
      "lat": 29.6699787, 
      "lng": 34.6331227
    }, 
    "objects": [
      {
        "images": [
          "1990-hotel-red-beach-sinai.jpg"
        ], 
        "object": "1990 Hotel Red Beach, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "Smederevo, Serbia": {
    "position": {
      "lat": 44.6658941, 
      "lng": 20.9335169
    }, 
    "objects": [
      {
        "images": [
          "1979-medical-centre-smederevo.jpg"
        ], 
        "object": "1979 Medical Centre, 45.000 m2, project, 1st award at the YU competition", 
        "area": 45000
      }
    ]
  }, 
  "Kharkow, Ukraine": {
    "position": {
      "lat": 49.9935, 
      "lng": 36.230383
    }, 
    "objects": [
      {
        "images": [
          "1997-hotel-kharkowski-kharkow.jpg"
        ], 
        "object": "1997 Hotel Kharkowski, 13.000 m2, project", 
        "area": 13000
      }
    ]
  }, 
  "Vodice, Slovenia": {
    "position": {
      "lat": 46.1896643, 
      "lng": 14.493854
    }, 
    "objects": [
      {
        "images": [
          "2010-shopping-center-vodice.jpg"
        ], 
        "object": "2010 Shopping center", 
        "area": 0
      }
    ]
  }, 
  "Karbala, Iraq": {
    "position": {
      "lat": 32.6068464, 
      "lng": 44.0103922
    }, 
    "objects": [
      {
        "images": [
          "2012-university-hospital-karbala-iraq.jpg"
        ], 
        "object": "2012 University Hospital, 94.000 m2, project", 
        "area": 94000
      }
    ]
  }, 
  "Portoro\u017e, Slovenia": {
    "position": {
      "lat": 45.5142898, 
      "lng": 13.5908455
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1997 Hotel Palace, 13.000 m2, project", 
        "area": 13000
      }, 
      {
        "images": [], 
        "object": "2004 Hotel Palace, 34.000 m2, project", 
        "area": 34000
      }, 
      {
        "images": [
          "2008-hotel-kempinski-palace-portoro.jpg"
        ], 
        "object": "2008 Hotel Kempinski palace, 28.000 m2, constructed 2008", 
        "area": 28000
      }
    ]
  }, 
  "Dom\u017eale, Slovenia": {
    "position": {
      "lat": 46.1419417, 
      "lng": 14.5944171
    }, 
    "objects": [
      {
        "images": [
          "1975-retirement-home-domale.jpg"
        ], 
        "object": "1975 Retirement Home, 4.600 m2, constructed 1977, Preseren's fund award 1979", 
        "area": 4600
      }, 
      {
        "images": [
          "1976-toko-factory-domale.jpg"
        ], 
        "object": "1976 Toko Factory, 3.500 m2, constructed 1977, Preseren's fund award 1979", 
        "area": 3500
      }, 
      {
        "images": [], 
        "object": "1977 Department Store, 7.000 m2, constructed 1977", 
        "area": 7000
      }, 
      {
        "images": [], 
        "object": "1978 Business Centre, 12.000 m2, constructed 1988", 
        "area": 12000
      }, 
      {
        "images": [
          "2000-bank-domale.jpg"
        ], 
        "object": "2000 Bank, 3.800 m2, constructed 2002", 
        "area": 3800
      }, 
      {
        "images": [
          "2009-residential-complex-domale.jpg"
        ], 
        "object": "2009 Residential complex", 
        "area": 0
      }
    ]
  }, 
  "Lipica, Slovenia": {
    "position": {
      "lat": 45.66698419999999, 
      "lng": 13.8809172
    }, 
    "objects": [
      {
        "images": [
          "2010-maestoso-hotel-lipica.jpg"
        ], 
        "object": "2010 Maestoso hotel", 
        "area": 0
      }, 
      {
        "images": [
          "2012-hotel-klub-lipica.jpg"
        ], 
        "object": "2012 Hotel \u00abKlub\u00bb, reconstruction", 
        "area": 0
      }
    ]
  }, 
  "Marienburg, Germany": {
    "position": {
      "lat": 52.1725307, 
      "lng": 9.766576599999999
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1995 Psychiatric Hospital, 15.000 m2, project", 
        "area": 15000
      }
    ]
  }, 
  "Almaty, Kazakhstan": {
    "position": {
      "lat": 43.2220146, 
      "lng": 76.8512485
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Almabank, 21.000 m2, project", 
        "area": 21000
      }, 
      {
        "images": [], 
        "object": "1994 Business Centre, 11.000 m2, project", 
        "area": 11000
      }, 
      {
        "images": [], 
        "object": "1999 Opera House- reconstruction, project", 
        "area": 0
      }, 
      {
        "images": [], 
        "object": "2006 Residential Complex Jubileini, 40.000 m2, project", 
        "area": 40000
      }, 
      {
        "images": [
          "2006-university-city-technopolis-almaty.jpg"
        ], 
        "object": "2006 University City \u00abTechnopolis\u00bb, 190.000 m2, project", 
        "area": 190000
      }
    ]
  }, 
  "Nitva, Russia": {
    "position": {
      "lat": 57.9331563, 
      "lng": 55.34672
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1990 Rehabilitation Centre, 21.000 m2, project", 
        "area": 21000
      }
    ]
  }, 
  "Constantine, Algeria": {
    "position": {
      "lat": 36.360155, 
      "lng": 6.642433
    }, 
    "objects": [
      {
        "images": [
          "1984-general-hospital-constantine.jpg"
        ], 
        "object": "1984 General Hospital, 23.000 m2, project", 
        "area": 23000
      }
    ]
  }, 
  "Tashkent, Uzbekistan": {
    "position": {
      "lat": 41.2994958, 
      "lng": 69.2400734
    }, 
    "objects": [
      {
        "images": [
          "1996-commercial-business-centre-tashkent.jpg"
        ], 
        "object": "1996 Commercial-Business Centre, 33.000 m2, project", 
        "area": 33000
      }, 
      {
        "images": [
          "1996-insurance-companies-tashkent.jpg"
        ], 
        "object": "1996 Insurance Companies, 26.000 m2, project", 
        "area": 26000
      }, 
      {
        "images": [], 
        "object": "1996 Headquarter Hokimijat, 11.200 m2, project", 
        "area": 11200
      }, 
      {
        "images": [], 
        "object": "1996 Bank-Akademia, 4.500 m2, constructed 1997", 
        "area": 4500
      }, 
      {
        "images": [], 
        "object": "1996 Hotel Bilder Saj, 2.600 m2, constructed 1998", 
        "area": 2600
      }
    ]
  }, 
  "Zrenjanin, Serbia": {
    "position": {
      "lat": 45.3815612, 
      "lng": 20.3685738
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1976 Medical Centre, 48.000 m2, constructed 1990, 1st award at the YU competition", 
        "area": 48000
      }, 
      {
        "images": [], 
        "object": "1977 Youth Palace, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [
          "1984-pub-house-zrenjanin.jpg"
        ], 
        "object": "1984 Pub House, 2.000 m2, project", 
        "area": 2000
      }
    ]
  }, 
  "Tatarstan, Russia": {
    "position": {
      "lat": 55.1802364, 
      "lng": 50.7263945
    }, 
    "objects": [
      {
        "images": [
          "1997-hotel-kazan-kazan.jpg"
        ], 
        "object": "1997 Hotel Kazan, Kazan, 33.000 m2, project", 
        "area": 33000
      }
    ]
  }, 
  "Savudria, Croatia": {
    "position": {
      "lat": 45.49251, 
      "lng": 13.5059259
    }, 
    "objects": [
      {
        "images": [
          "2011-hotel-and-resort-vila-savudria.jpg"
        ], 
        "object": "2011 Hotel and resort vila", 
        "area": 0
      }
    ]
  }, 
  "Perm, Russia": {
    "position": {
      "lat": 58.02968129999999, 
      "lng": 56.2667916
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1997 Infection Clinic, 50.000 m2, project", 
        "area": 50000
      }
    ]
  }, 
  "Iraq": {
    "position": {
      "lat": 33.223191, 
      "lng": 43.679291
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2001 General Hospital- typical, 16.000 m2, project", 
        "area": 16000
      }, 
      {
        "images": [], 
        "object": "2001 Pediatric and Maternity Polyclinic- typical, 8.000 m2, project", 
        "area": 8000
      }
    ]
  }, 
  "Abudabi, United Arab Emirates": {
    "position": {
      "lat": 24.2991738, 
      "lng": 54.6972774
    }, 
    "objects": [
      {
        "images": [
          "2005-hotel-aquatel-abudabi.jpg"
        ], 
        "object": "2005 Hotel Aquatel, 40.200 m2, project, 1st award at the internal competition", 
        "area": 40200
      }, 
      {
        "images": [], 
        "object": "2005 Condominium Tower, 40.450 m2, project", 
        "area": 40450
      }, 
      {
        "images": [
          "2005-entertainment-tourist-complex-arabic-park.jpg"
        ], 
        "object": "2005 Entertainment Tourist Complex, Arabic Park, 350.000 m2, project", 
        "area": 350000
      }
    ]
  }, 
  "Dniprodzerzhynsk, Ukraine": {
    "position": {
      "lat": 48.523117, 
      "lng": 34.613683
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1992 Gynecology-Maternity Clinic, 12.000 m2, project 1992", 
        "area": 12000
      }, 
      {
        "images": [], 
        "object": "1993 Restaurant Unibros, 7.500 m2, project", 
        "area": 7500
      }
    ]
  }, 
  "Rabac, Croatia": {
    "position": {
      "lat": 45.0781668, 
      "lng": 14.1617033
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1978 Tourist Hotel Recreation Complex, 60.000 m2, project", 
        "area": 60000
      }
    ]
  }, 
  "Yekaterinburg, Russia": {
    "position": {
      "lat": 56.83892609999999, 
      "lng": 60.6057025
    }, 
    "objects": [
      {
        "images": [
          "2005-supermarkets-kirovski-yekaterinburg.jpg"
        ], 
        "object": "2005 Supermarkets Kirovski, project", 
        "area": 0
      }, 
      {
        "images": [
          "2006-shopping-centre-kirovsky-ozero-yekaterinburg.jpg"
        ], 
        "object": "2006 Shopping Centre \u00abKirovsky Ozero\u00bb, 20.300 m2, project", 
        "area": 20300
      }, 
      {
        "images": [
          "2007-commercial-recreation-centre-patrusi.jpg"
        ], 
        "object": "2007 Commercial-Recreation Centre, Patrusi, 93.000 m2, project", 
        "area": 93000
      }, 
      {
        "images": [], 
        "object": "2007 Residential Complex, Patrusi, 430.990 m2, project 2007", 
        "area": 430990
      }, 
      {
        "images": [], 
        "object": "2007 Residential-Business Building, 79.000 m2, project", 
        "area": 79000
      }, 
      {
        "images": [], 
        "object": "2007 Aqua Park G. Pishma, 24.800 m2, project", 
        "area": 24800
      }
    ]
  }, 
  "Ulcinj, Montenegro": {
    "position": {
      "lat": 41.9310884, 
      "lng": 19.2147632
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1989 Hotel Dvorac, 16.000 m2, project", 
        "area": 16000
      }
    ]
  }, 
  "Bugojno, Bosnia and Herzegovina": {
    "position": {
      "lat": 44.0562486, 
      "lng": 17.4498275
    }, 
    "objects": [
      {
        "images": [
          "1979-medical-centre-bugojno.jpg"
        ], 
        "object": "1979 Medical Centre, 28.000 m2, under construction", 
        "area": 28000
      }
    ]
  }, 
  "Budapest, Hungary": {
    "position": {
      "lat": 47.497912, 
      "lng": 19.040235
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1996 Hotel Roszadomb, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [], 
        "object": "1999 Aqua Park, 15.000 m2, project", 
        "area": 15000
      }, 
      {
        "images": [], 
        "object": "2007 Entertainment Tourist Complex Ecopark Europa, 300.000 m2, project", 
        "area": 300000
      }
    ]
  }, 
  "Nukus, Uzbekistan": {
    "position": {
      "lat": 42.461891, 
      "lng": 59.6166312
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2000 Residence of President, 1.900 m2, project", 
        "area": 1900
      }
    ]
  }, 
  "Moscow, Russia": {
    "position": {
      "lat": 55.755826, 
      "lng": 37.6173
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1987 General Hospital Botkin, 79.000 m2, project", 
        "area": 79000
      }, 
      {
        "images": [], 
        "object": "1988 Hotel Mezdunarodnaja, 7.000 m2, project", 
        "area": 7000
      }, 
      {
        "images": [
          "1991-neurosurgery-clinic-moscow.jpg"
        ], 
        "object": "1991 Neurosurgery Clinic, 95.000 m2, constructed 1997, Moscow city diploma for the best realization in the period 1990\u20132000", 
        "area": 95000
      }, 
      {
        "images": [
          "1992-theatre-and-concert-centre-arbat.jpg"
        ], 
        "object": "1992 Theatre and Concert Centre, Arbat, 5.600 m2, constructed 1995", 
        "area": 5600
      }, 
      {
        "images": [], 
        "object": "1993 Headquarter Enico, 12.000 m2, project", 
        "area": 12000
      }, 
      {
        "images": [
          "1994-white-house-moscow-russia.jpg"
        ], 
        "object": "1994 White House, interior design, constructed 1994", 
        "area": 0
      }, 
      {
        "images": [
          "1994-hotel-golden-ring-moscow.jpg"
        ], 
        "object": "1994 Hotel Golden Ring, 37.000 m2, constructed 1998", 
        "area": 37000
      }, 
      {
        "images": [
          "1994-business-centre-reforma-moscow.jpg"
        ], 
        "object": "1994 Business Centre Reforma, 65.000 m2, constructed 2001", 
        "area": 65000
      }, 
      {
        "images": [
          "1995-oncology-hospital-krasnogorsk.jpg"
        ], 
        "object": "1995 Oncology Hospital, Krasnogorsk, 19.300 m2, constructed 2001", 
        "area": 19300
      }, 
      {
        "images": [
          "1995-business-centre-meta-dom.jpg"
        ], 
        "object": "1995 Business Centre, \u00abMeta Dom\u00bb, 26.000 m2, constructed 1997", 
        "area": 26000
      }, 
      {
        "images": [
          "1995-presidential-residence-rf-moscow.jpg"
        ], 
        "object": "1995 Presidential Residence RF, project", 
        "area": 0
      }, 
      {
        "images": [], 
        "object": "1995 Headquarter Neftehim Bank, 14.000 m2, project", 
        "area": 14000
      }, 
      {
        "images": [], 
        "object": "1995 Bank- Eurosibbank, 8.000 m2, project", 
        "area": 8000
      }, 
      {
        "images": [
          "1997-airport-vnukovo-moscow.jpg"
        ], 
        "object": "1997 Airport Vnukovo, 7.500 m2, project", 
        "area": 7500
      }, 
      {
        "images": [], 
        "object": "1997 School Centre AKB AGRO Bank, 7.200 m2, project", 
        "area": 7200
      }, 
      {
        "images": [], 
        "object": "1997 Commercial Centre Zepter, 1.500 m2, project", 
        "area": 1500
      }, 
      {
        "images": [
          "1998-zepter-commercial-shopping-centre-moscow.jpg"
        ], 
        "object": "1998 Zepter Commercial-Shopping Centre, 4.000 m2, project", 
        "area": 4000
      }, 
      {
        "images": [
          "1998-aqua-centre-jasenowo-moscow.jpg"
        ], 
        "object": "1998 Aqua Centre Jasenowo, 19.500 m2, constructed 2001", 
        "area": 19500
      }, 
      {
        "images": [
          "2000-multi-purpose-apartment-complex-severnosijanie.jpg"
        ], 
        "object": "2000 Multi-purpose Apartment Complex, Severnosijanie, 130.000 m2, project", 
        "area": 130000
      }, 
      {
        "images": [
          "2000-business-centre-rao.jpg"
        ], 
        "object": "2000 Business Centre, RAO N.N., 47 .000 m2, project", 
        "area": 47000
      }, 
      {
        "images": [
          "2000-business-centre-saharova-moscow.jpg"
        ], 
        "object": "2000 Business Centre Saharova, 110.000 m2, project, 1st award at the internal competition", 
        "area": 110000
      }, 
      {
        "images": [], 
        "object": "2001 Apartments Complex Troick, 34.500 m2, project", 
        "area": 34500
      }, 
      {
        "images": [
          "2001-business-centre-and-hotel-katarina-moscow.jpg"
        ], 
        "object": "2001 Business Centre and Hotel Katarina, 50.000 m2, project", 
        "area": 50000
      }, 
      {
        "images": [
          "2002-hotel-peter-i-moscow-russia.jpg"
        ], 
        "object": "2002 Hotel \u00abPeter I\u00bb, 29.000 m2, constructed 2005", 
        "area": 29000
      }, 
      {
        "images": [], 
        "object": "2002 Shopping Centre, Butovo, 26.600 m2, project", 
        "area": 26600
      }, 
      {
        "images": [], 
        "object": "2003 Aqua Park Zavidovo, 3.700 m2, project", 
        "area": 3700
      }, 
      {
        "images": [
          "2003-apartment-complex-ostozhenka-moscow.jpg"
        ], 
        "object": "2003 Apartment Complex Ostozhenka, 50.000 m2, project", 
        "area": 50000
      }, 
      {
        "images": [], 
        "object": "2003 Business Centre \u00abKrasnoproletarskaya\u00bb, 5.000 m2, constructed 2006", 
        "area": 5000
      }, 
      {
        "images": [], 
        "object": "2004 General Hospital, Balasiki, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [
          "2007-residential-business-complex-arbat-moscow.jpg"
        ], 
        "object": "2007 Residential-Business Complex Arbat, 58.400 m2, project", 
        "area": 58400
      }, 
      {
        "images": [
          "2007-hotel-recreation-complex-olenkur-moscow.jpg"
        ], 
        "object": "2007 Hotel-Recreation Complex Olenkur, 84.500 m2, project", 
        "area": 84500
      }, 
      {
        "images": [
          "2007-logistic-centre-chehow-moscow.jpg"
        ], 
        "object": "2007 Logistic Centre \u00abChehow\u00bb, 30.000 m2, constructed 2010", 
        "area": 30000
      }, 
      {
        "images": [
          "2007-crown-plaza-hotel-moscow.jpg"
        ], 
        "object": "2007 Crown Plaza Hotel, 15.800 m2, design project, constructed 2010", 
        "area": 15800
      }, 
      {
        "images": [
          "2008-management-teaching-centre-transaero-moscow.jpg"
        ], 
        "object": "2008 Management-Teaching Centre \u00abTransaero\u00bb, 38.000 m2, project", 
        "area": 38000
      }, 
      {
        "images": [
          "2008-shopping-centre-xxi-century-moscow.jpg"
        ], 
        "object": "2008 Shopping Centre \u00abXXI Century\u00bb, 57.000 m2, project", 
        "area": 57000
      }, 
      {
        "images": [
          "2013-hotel-radonezskii-moscow.jpg"
        ], 
        "object": "2013 Hotel \u00abRadonezskii\u00bb, 9.000 m2, project", 
        "area": 9000
      }, 
      {
        "images": [
          "2003-business-centre-moscow.jpg"
        ], 
        "object": "2003 Business Centre", 
        "area": 0
      }
    ]
  }, 
  "Tr\u017ei\u010d, Slovenia": {
    "position": {
      "lat": 46.36215170000001, 
      "lng": 14.3083372
    }, 
    "objects": [
      {
        "images": [
          "2013-outdoor-swimming-pool-tri.jpg"
        ], 
        "object": "2013 Outdoor swimming pool", 
        "area": 0
      }
    ]
  }, 
  "Adzarija, Georgia": {
    "position": {
      "lat": 41.6005626, 
      "lng": 42.0688383
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1999 Hotel Kolheti, 29.000 m2, project", 
        "area": 29000
      }
    ]
  }, 
  "Kranj, Slovenia": {
    "position": {
      "lat": 46.2428344, 
      "lng": 14.3555417
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2011 Library Globus, 11.500 m2, constructed 2011", 
        "area": 11500
      }, 
      {
        "images": [
          "2011-city-library-and-office-building-kranj.jpg"
        ], 
        "object": "2011 City library and office building", 
        "area": 0
      }, 
      {
        "images": [
          "2013-lon-bank-building-kranj.jpg"
        ], 
        "object": "2013 \u00abLon\u00bb bank building", 
        "area": 0
      }
    ]
  }, 
  "S\u00e3o Tome e Principe": {
    "position": {
      "lat": 0.18636, 
      "lng": 6.613080999999999
    }, 
    "objects": [
      {
        "images": [
          "1984-hotel-miramare-so-tome-e-principe.jpg"
        ], 
        "object": "1984 Hotel Miramare, 9.000 m2, constructed 1988", 
        "area": 9000
      }, 
      {
        "images": [
          "1984-hotel-tourist-resort-so-tome-e-principe.jpg"
        ], 
        "object": "1984 Hotel-Tourist Resort, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "Troick, Russia": {
    "position": {
      "lat": 54.0747574, 
      "lng": 61.56705100000001
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1999 Polyclinic, 15.000 m2, project", 
        "area": 15000
      }, 
      {
        "images": [], 
        "object": "1999 Baby Food Factory, 7.000 m2, project", 
        "area": 7000
      }
    ]
  }, 
  "Saranda, Albania": {
    "position": {
      "lat": 39.8592119, 
      "lng": 20.0271001
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Nautical Centre, 6.000 m2, project", 
        "area": 6000
      }
    ]
  }, 
  "Limassol, Cyprus": {
    "position": {
      "lat": 34.7071301, 
      "lng": 33.0226174
    }, 
    "objects": [
      {
        "images": [
          "1993-unibros-business-centre-limassol.jpg"
        ], 
        "object": "1993 Unibros Business Centre, 2.500 m2, project", 
        "area": 2500
      }
    ]
  }, 
  "Koksaray, Kazakhstan": {
    "position": {
      "lat": 42.6564432, 
      "lng": 68.1491034
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2006 Residential Complex, 5 .000 m2, constructed 2009", 
        "area": 5000
      }
    ]
  }, 
  "Miren, Slovenia": {
    "position": {
      "lat": 45.89537379999999, 
      "lng": 13.6081681
    }, 
    "objects": [
      {
        "images": [
          "2010-cultural-centre-miren.jpg"
        ], 
        "object": "2010 Cultural centre", 
        "area": 0
      }
    ]
  }, 
  "\u010cate\u017e, Slovenia": {
    "position": {
      "lat": 45.9698392, 
      "lng": 14.9603746
    }, 
    "objects": [
      {
        "images": [
          "1997-aqua-park-and-hotel-ate.jpg"
        ], 
        "object": "1997 Aqua Park and Hotel, 23.400 m2, project", 
        "area": 23400
      }, 
      {
        "images": [], 
        "object": "2006 Aqua Park III. Dom, 2.200 m2, project", 
        "area": 2200
      }
    ]
  }, 
  "Djibouti": {
    "position": {
      "lat": 11.825138, 
      "lng": 42.590275
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1985 Hotel-Business Centre, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "Warsaw, Poland": {
    "position": {
      "lat": 52.2296756, 
      "lng": 21.0122287
    }, 
    "objects": [
      {
        "images": [
          "1987-business-premises-warsaw.jpg"
        ], 
        "object": "1987 Business premises, 40.000 m2, constructed 1992", 
        "area": 40000
      }, 
      {
        "images": [
          "1990-passage-korasiego-warsaw.jpg"
        ], 
        "object": "1990 Passage Korasiego, 7.000 m2, project", 
        "area": 7000
      }, 
      {
        "images": [
          "1993-headquarter-zepter-warsaw.jpg"
        ], 
        "object": "1993 Headquarter Zepter, 8.000 m2, project", 
        "area": 8000
      }
    ]
  }, 
  "Kranjska Gora, Slovenia": {
    "position": {
      "lat": 46.4845293, 
      "lng": 13.7857145
    }, 
    "objects": [
      {
        "images": [
          "1986-hotel-kompas-kranjska-gora.jpg"
        ], 
        "object": "1986 Hotel Kompas, 15.000 m2, project", 
        "area": 15000
      }, 
      {
        "images": [
          "1998-hotel-and-casino-kranjska-gora.jpg"
        ], 
        "object": "1998 Hotel and Casino, 7.000 m2, constructed 2000", 
        "area": 7000
      }, 
      {
        "images": [
          "2011-hotel-and-casino-korona-kranjska-gora.jpg"
        ], 
        "object": "2011 Hotel and casino \u00abKorona\u00bb", 
        "area": 0
      }
    ]
  }, 
  "Banja Luka, Bosnia and Herzegovina": {
    "position": {
      "lat": 44.7721811, 
      "lng": 17.191
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1968 Shopping Centre, 70.000 m2, project", 
        "area": 70000
      }, 
      {
        "images": [
          "1969-shopping-centre-banja-luka.jpg"
        ], 
        "object": "1969 Shopping Centre", 
        "area": 0
      }
    ]
  }, 
  "St. Petersburg, Russia": {
    "position": {
      "lat": 59.9342802, 
      "lng": 30.3350986
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2002 Aqua Park Primskovo, 27.000 m2, project", 
        "area": 27000
      }, 
      {
        "images": [], 
        "object": "2003 Recreation and Hotel Centre Repino, 37.500 m2, project", 
        "area": 37500
      }, 
      {
        "images": [
          "2003-recreation-centre-st.jpg"
        ], 
        "object": "2003 Recreation Centre", 
        "area": 0
      }
    ]
  }, 
  "Berlin, Germany": {
    "position": {
      "lat": 52.52000659999999, 
      "lng": 13.404954
    }, 
    "objects": [
      {
        "images": [
          "1989-hospital-rudolf-wirchow-berlin.jpg"
        ], 
        "object": "1989 Hospital Rudolf Wirchow, 35.000 m2, project", 
        "area": 35000
      }, 
      {
        "images": [
          "1992-psychiatric-clinic-berlin.jpg"
        ], 
        "object": "1992 Psychiatric Clinic, 23.000 m2, project", 
        "area": 23000
      }
    ]
  }, 
  "Katarina, Slovenia": {
    "position": {
      "lat": 45.4875, 
      "lng": 14.2961111
    }, 
    "objects": [
      {
        "images": [
          "2011-riding-hall-katarina.jpg"
        ], 
        "object": "2011 Riding hall", 
        "area": 0
      }
    ]
  }, 
  "Bukovnica, Slovenia": {
    "position": {
      "lat": 46.6916572, 
      "lng": 16.3236343
    }, 
    "objects": [
      {
        "images": [
          "1993-hotel-and-health-resort-bukovnica.jpg"
        ], 
        "object": "1993 Hotel and Health Resort, 56.000 m2, project, 1st award at the internal competition", 
        "area": 56000
      }
    ]
  }, 
  "Hirschberg, Germany": {
    "position": {
      "lat": 49.508533, 
      "lng": 8.655790099999999
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1992 Hotel Supol, 3.000 m2, project", 
        "area": 3000
      }
    ]
  }, 
  "Belo Ozero, Russia": {
    "position": {
      "lat": 56.335, 
      "lng": 28.7711111
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2000 Sanatorium, 22.000 m2, project", 
        "area": 22000
      }
    ]
  }, 
  "Pula, Croatia": {
    "position": {
      "lat": 44.8666232, 
      "lng": 13.8495788
    }, 
    "objects": [
      {
        "images": [
          "1970-tourist-hotel-recreational-complex-medulin.jpg"
        ], 
        "object": "1970 Tourist Hotel-Recreational Complex, Medulin, 120.000 m2, project", 
        "area": 120000
      }
    ]
  }, 
  "Varadera, Cuba": {
    "position": {
      "lat": 23.179858, 
      "lng": -81.1885293
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1989 Hotel Aurora, 15.000 m2, project", 
        "area": 15000
      }
    ]
  }, 
  "Bucharest, Romania": {
    "position": {
      "lat": 44.4267674, 
      "lng": 26.1025384
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1994 General Hospital, 12.000 m2, project", 
        "area": 12000
      }, 
      {
        "images": [], 
        "object": "1994 Cardiovascular Centre, 25.000 m2, project", 
        "area": 25000
      }, 
      {
        "images": [], 
        "object": "1995 General Hospital, 9.100 m2, project", 
        "area": 9100
      }
    ]
  }, 
  "Amman, Jordan": {
    "position": {
      "lat": 31.9565783, 
      "lng": 35.9456951
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1985 Royal Academy, 22.000 m2, project", 
        "area": 22000
      }, 
      {
        "images": [
          "1985-jordan-university-hall-amman.jpg"
        ], 
        "object": "1985 Jordan University Hall, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [], 
        "object": "1985 King Abdulah Mosque, 5.000 m2, project", 
        "area": 5000
      }
    ]
  }, 
  "Sakha, Russia": {
    "position": {
      "lat": 66.7613451, 
      "lng": 124.1237531
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Regional Supply and Shopping Centers- typical, 5.100 m2, project", 
        "area": 5100
      }, 
      {
        "images": [], 
        "object": "1993 General Hospital, Eveno- Bitantajskaja, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [
          "1994-regional-hospital-centers-xxi-century.jpg"
        ], 
        "object": "1994 Regional Hospital Centers, XXI Century, 10.000-16.000 m2, project", 
        "area": 16000
      }, 
      {
        "images": [], 
        "object": "1995 Maternity Hospital, Pokrovsk, 4.000 m2, constructed 1997", 
        "area": 4000
      }, 
      {
        "images": [], 
        "object": "1998 Regional Hospitals-typical, 12.000 m2, project", 
        "area": 12000
      }
    ]
  }, 
  "Ust-Kamenogorsk, Kazakhstan": {
    "position": {
      "lat": 49.97492949999999, 
      "lng": 82.6017244
    }, 
    "objects": [
      {
        "images": [
          "1989-general-hospital-ust-kamenogorsk.jpg"
        ], 
        "object": "1989 General Hospital, 75.000 m2, constructed 1993, 1st award at the internal competition", 
        "area": 75000
      }
    ]
  }, 
  "Lvov, Ukraine": {
    "position": {
      "lat": 49.839683, 
      "lng": 24.029717
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2003 Mega Cinema Centre, 12.500 m2, project", 
        "area": 12500
      }
    ]
  }, 
  "Bayreuth, Lebanon": {
    "position": {
      "lat": 33.8923706, 
      "lng": 35.511069
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1994 Hotel Fegali, 7.700 m2, project", 
        "area": 7700
      }
    ]
  }, 
  "Baghdad, Iraq": {
    "position": {
      "lat": 33.3128057, 
      "lng": 44.3614875
    }, 
    "objects": [
      {
        "images": [
          "1982-cultural-recreational-centre-baghdad.jpg"
        ], 
        "object": "1982 Cultural-Recreational Centre, 40.000 m2, constructed 1986, 1st award at the internal competition", 
        "area": 40000
      }, 
      {
        "images": [
          "1989-presidential-palace-alkadia-baghdad.jpg"
        ], 
        "object": "1989 Presidential Palace Alkadia, 22.000 m2, project", 
        "area": 22000
      }, 
      {
        "images": [], 
        "object": "1993 National opera house, 27.000 m2, project", 
        "area": 27000
      }
    ]
  }, 
  "Yakutsk, Russia": {
    "position": {
      "lat": 62.0354523, 
      "lng": 129.6754745
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Conference Hall, 700 m2, constructed 1993", 
        "area": 0
      }, 
      {
        "images": [
          "1993-centre-of-mother-and-child-care-yakutsk.jpg"
        ], 
        "object": "1993 Centre of Mother and Child Care, 55.000 m2, constructed 1997, 1st award at the internal competition", 
        "area": 55000
      }, 
      {
        "images": [], 
        "object": "1993 Hotel President, 3.900 m2, constructed 1994", 
        "area": 3900
      }, 
      {
        "images": [], 
        "object": "1993 Kindergarten Central, 2.500 m2, project", 
        "area": 2500
      }, 
      {
        "images": [
          "1993-wtc-business-centre-yakutsk.jpg"
        ], 
        "object": "1993 WTC Business Centre, 21.400 m2, constructed 1997", 
        "area": 21400
      }, 
      {
        "images": [
          "1993-headquarter-almazi-yakutsk.jpg"
        ], 
        "object": "1993 Headquarter Almazi, 7.000 m2, constructed 1997, Russian architects association degree for the best architectural achievement in years 1998 \u2013 2000", 
        "area": 7000
      }, 
      {
        "images": [], 
        "object": "1994 Wastewater facility, 15.000 m2, constructed 1999", 
        "area": 15000
      }, 
      {
        "images": [], 
        "object": "1994 Commercial-Residential complex, 13.000 m2, project", 
        "area": 13000
      }
    ]
  }, 
  "Kremenchuk, Ukraine": {
    "position": {
      "lat": 49.065783, 
      "lng": 33.410033
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1991 Rehabilitation Centre, 11.500 m2, project", 
        "area": 11500
      }
    ]
  }, 
  "Zimbabwe": {
    "position": {
      "lat": -19.015438, 
      "lng": 29.154857
    }, 
    "objects": [
      {
        "images": [
          "1999-regional-health-care-units-typical-zimbabwe.jpg"
        ], 
        "object": "1999 Regional health care units-typical, 500-2.000 m2, project", 
        "area": 2000
      }
    ]
  }, 
  "Ubas Labin, Croatia": {
    "position": {
      "lat": 45.091589, 
      "lng": 14.1238049
    }, 
    "objects": [
      {
        "images": [
          "1970-tourist-hotel-recreational-complex-ubas-labin.jpg"
        ], 
        "object": "1970 Tourist Hotel-Recreational Complex, 180.000 m2, project", 
        "area": 180000
      }
    ]
  }, 
  "typical, Iran": {
    "position": {
      "lat": 36.2446482, 
      "lng": 49.9828436
    }, 
    "objects": [
      {
        "images": [
          "2006-petrol-services-typical.jpg"
        ], 
        "object": "2006 Petrol Services, 2.000 m2, project", 
        "area": 2000
      }
    ]
  }, 
  "Volgograd, Russia": {
    "position": {
      "lat": 48.708048, 
      "lng": 44.5133034
    }, 
    "objects": [
      {
        "images": [
          "1993-bank-and-museum-volgograd.jpg"
        ], 
        "object": "1993 Bank and Museum, 22.500 m2, constructed 1996, International Architecture Academy degree, Russia Federation, 1995", 
        "area": 22500
      }, 
      {
        "images": [
          "1993-business-centre-nedvienost-volgograd.jpg"
        ], 
        "object": "1993 Business Centre Nedvi\u017eenost, 19.000 m2, project", 
        "area": 19000
      }
    ]
  }, 
  "Becici, Montenegro": {
    "position": {
      "lat": 42.2841511, 
      "lng": 18.8744587
    }, 
    "objects": [
      {
        "images": [
          "2007-apartment-villas-becici.jpg"
        ], 
        "object": "2007 Apartment, Villas, 20.000 m2, project", 
        "area": 20000
      }
    ]
  }, 
  "Groznyy, Russia": {
    "position": {
      "lat": 43.3168796, 
      "lng": 45.68148559999999
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1995 General Hospital, 55.000 m2, project", 
        "area": 55000
      }, 
      {
        "images": [], 
        "object": "1995 Hotel, 7.000 m2, project", 
        "area": 7000
      }
    ]
  }, 
  "Dobrna, Slovenia": {
    "position": {
      "lat": 46.3356141, 
      "lng": 15.2259732
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2005 Thermal spa Dobrna, 6.000 m2, constructed 2005", 
        "area": 6000
      }
    ]
  }, 
  "Rogatec, Slovenia": {
    "position": {
      "lat": 46.2258891, 
      "lng": 15.7000313
    }, 
    "objects": [
      {
        "images": [
          "2011-plant-for-botteling-water-rogatec.jpg"
        ], 
        "object": "2011 Plant for Botteling water", 
        "area": 0
      }
    ]
  }, 
  "Monte Barreto, Cuba": {
    "position": {
      "lat": 23.1050877, 
      "lng": -82.4366816
    }, 
    "objects": [
      {
        "images": [
          "1987-hotel-tourist-centre-monte-barreto.jpg"
        ], 
        "object": "1987 Hotel-Tourist Centre, 440.000 m2, project, 1st award at the internal competition", 
        "area": 440000
      }
    ]
  }, 
  "Orenburg, Russia": {
    "position": {
      "lat": 51.7666482, 
      "lng": 55.1004538
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Airport, 3.000 m2, project", 
        "area": 3000
      }, 
      {
        "images": [], 
        "object": "1994 Pediatric Oncology, 9.000 m2, project", 
        "area": 9000
      }
    ]
  }, 
  "Leipzig, Germany": {
    "position": {
      "lat": 51.3396955, 
      "lng": 12.3730747
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1991 Agrarian-Business Centre, 91 000 m2, project", 
        "area": 0
      }, 
      {
        "images": [], 
        "object": "1992 Hotel Minol, 9.000 m2, project", 
        "area": 9000
      }
    ]
  }, 
  "Norilsk, Russia": {
    "position": {
      "lat": 69.35579, 
      "lng": 88.18929380000002
    }, 
    "objects": [
      {
        "images": [
          "1988-general-hospital-norilsk.jpg"
        ], 
        "object": "1988 General Hospital, 62.500 m2, constructed 1993", 
        "area": 62500
      }, 
      {
        "images": [], 
        "object": "1993 Maternity Hospital, 9.000 m2, project", 
        "area": 9000
      }, 
      {
        "images": [
          "1993-residential-commercial-complex-norilsk.jpg"
        ], 
        "object": "1993 Residential-Commercial Complex, 40.000 m2, under construction", 
        "area": 40000
      }, 
      {
        "images": [], 
        "object": "1995 Residential-Commercial Premises, 37.000 m2, constructed 2001", 
        "area": 37000
      }, 
      {
        "images": [], 
        "object": "1996 Administration Building, N. N. Company, 13.700 m2, project", 
        "area": 13700
      }, 
      {
        "images": [
          "1996-resident-business-recreation-complex-norilsk.jpg"
        ], 
        "object": "1996 Resident-Business-Recreation Complex, 73.700 m2, project", 
        "area": 73700
      }, 
      {
        "images": [], 
        "object": "1996 Business Centre Sberbank, 5.000 m2, project", 
        "area": 5000
      }, 
      {
        "images": [
          "2000-psychoneurology-and-narcology-hospital-norilsk.jpg"
        ], 
        "object": "2000 Psychoneurology and Narcology Hospital, 20.000 m2, constructed 2002", 
        "area": 20000
      }, 
      {
        "images": [
          "2000-hotel-norilsk.jpg"
        ], 
        "object": "2000 Hotel Norilsk VIP-reconstruction, 5.000 m2, constructed 2002", 
        "area": 5000
      }, 
      {
        "images": [], 
        "object": "2000 Cinema, 2.000 m2, project, constructed 2002", 
        "area": 2000
      }, 
      {
        "images": [
          "2001-recreation-and-shopping-centre-norilsk.jpg"
        ], 
        "object": "2001 Recreation and Shopping Centre, 35.000 m2, project", 
        "area": 35000
      }, 
      {
        "images": [], 
        "object": "2001 Infection Clinic, 6.500 m2, constructed 2004", 
        "area": 6500
      }, 
      {
        "images": [], 
        "object": "2002 Pediatric Clinic, 9.500 m2, constructed 2004", 
        "area": 9500
      }, 
      {
        "images": [
          "2002-bus-station-norilsk.jpg"
        ], 
        "object": "2002 Bus Station, 4.500 m2, constructed 2004", 
        "area": 4500
      }, 
      {
        "images": [
          "2002-aqua-park-norilsk.jpg"
        ], 
        "object": "2002 Aqua Park, 27.000 m2, project", 
        "area": 27000
      }, 
      {
        "images": [], 
        "object": "2005 Sports Centre Arena, 19.500 m2, project", 
        "area": 19500
      }
    ]
  }, 
  "Bohinj, Slovenia": {
    "position": {
      "lat": 46.30056520000001, 
      "lng": 13.9427195
    }, 
    "objects": [
      {
        "images": [
          "2009-hotel-belleveu-bohinj.jpg"
        ], 
        "object": "2009 Hotel Belleveu", 
        "area": 0
      }
    ]
  }, 
  "Nikitskoe, Russia": {
    "position": null, 
    "objects": [
      {
        "images": [
          "2013-hotel-and-health-resort-nikitskoe.jpg"
        ], 
        "object": "2013 Hotel and Health Resort Nikitskoe, 31.400 m2, project", 
        "area": 31400
      }
    ]
  }, 
  "Staryy Oskol, Russia": {
    "position": {
      "lat": 51.2980824, 
      "lng": 37.8379593
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1993 Hospital, 3.200 m2, constructed 1994", 
        "area": 3200
      }, 
      {
        "images": [], 
        "object": "1995 Polyclinic, 4.000 m2, constructed 1998", 
        "area": 4000
      }
    ]
  }, 
  "Bar, Montenegro": {
    "position": {
      "lat": 42.2040425, 
      "lng": 19.140438
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2009 Hotel Topolica, 13.700 m2, under construction", 
        "area": 13700
      }
    ]
  }, 
  "Ljubljana, Slovenia": {
    "position": {
      "lat": 46.0569465, 
      "lng": 14.5057515
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1968 Radio TV Centre, urban plan, project", 
        "area": 0
      }, 
      {
        "images": [
          "1972-radio-tv-centre-ljubljana.jpg"
        ], 
        "object": "1972 Radio TV Centre, 28.000 m2, project, selected in the 2nd stage of the competition", 
        "area": 28000
      }, 
      {
        "images": [
          "1974-military-high-school-ljubljana.jpg"
        ], 
        "object": "1974 Military High School, 4.500 m2, constructed 1975, Preseren's fund award 1979 and YU architecture salon award 1982", 
        "area": 4500
      }, 
      {
        "images": [], 
        "object": "1978 Crematorium, 6.400 m2, constructed 1981", 
        "area": 6400
      }, 
      {
        "images": [
          "1980-new-military-hospital-ljubljana.jpg"
        ], 
        "object": "1980 New Military Hospital, 58.000 m2, project, 1st award at the YU competition", 
        "area": 58000
      }, 
      {
        "images": [
          "1991-hotel-trnovo-ljubljana.jpg"
        ], 
        "object": "1991 Hotel Trnovo, 12.000 m2, project", 
        "area": 12000
      }, 
      {
        "images": [], 
        "object": "1991 Motel Jug, 8.000 m2, project", 
        "area": 8000
      }, 
      {
        "images": [
          "1992-residential-business-complex-barjanska-cesta-ljubljana.jpg"
        ], 
        "object": "1992 Residential-Business Complex Barjanska cesta, urban plan, project, narrow selection at the Slovenian competition", 
        "area": 0
      }, 
      {
        "images": [
          "1992-sports-centre-llirija-ljubljana.jpg"
        ], 
        "object": "1992 Sports Centre llirija, 14.000 m2, project", 
        "area": 14000
      }, 
      {
        "images": [
          "1996-oncology-clinic-ljubljana.jpg"
        ], 
        "object": "1996 Oncology Clinic, 42.700 m2, constructed 2007, 1st award at the Slovenian competition", 
        "area": 42700
      }, 
      {
        "images": [
          "1996-orl-clinic-ljubljana.jpg"
        ], 
        "object": "1996 ORL Clinic, 8.700 m2, constructed 2007, 1st award at the Slovenian competition", 
        "area": 8700
      }, 
      {
        "images": [
          "1996-residential-complex-kondominium-ljubljana.jpg"
        ], 
        "object": "1996 Residential Complex Kondominium, 40.000 m2, project", 
        "area": 40000
      }, 
      {
        "images": [
          "1997-lek-development-centre-ljubljana.jpg"
        ], 
        "object": "1997 LEK Development Centre, 9.700 m2, constructed 2001", 
        "area": 9700
      }, 
      {
        "images": [], 
        "object": "1997 Multiplex Cinema Celovska, 15.000 m2, project", 
        "area": 15000
      }, 
      {
        "images": [
          "1998-residential-complex-koseki-bajer-ljubljana.jpg"
        ], 
        "object": "1998 Residential Complex \u00abKose\u0161ki bajer\u00bb, 75.000 m2, constructed 2003", 
        "area": 75000
      }, 
      {
        "images": [
          "1998-mega-cinema-centre-btc-ljubljana.jpg"
        ], 
        "object": "1998 Mega Cinema Centre BTC, 12.600 m2, project", 
        "area": 12600
      }, 
      {
        "images": [
          "1998-pediatric-clinic-ljubljana.jpg"
        ], 
        "object": "1998 Pediatric Clinic, 26.000 m2, constructed 2008, 1st award at the competition", 
        "area": 26000
      }, 
      {
        "images": [
          "2000-business-centre-glass-palace-ljubljana.jpg"
        ], 
        "object": "2000 Business-Centre Glass Palace, 31.000 m2, project, constructed 2003", 
        "area": 31000
      }, 
      {
        "images": [], 
        "object": "2000 Ljubljana Fair ground, 45.000 m2, development project", 
        "area": 45000
      }, 
      {
        "images": [
          "2000-philharmonic-hall-ljubljana.jpg"
        ], 
        "object": "2000 Philharmonic Hall, reconstruction, 5.000 m2, constructed 2002, 1st award at the internal competition", 
        "area": 5000
      }, 
      {
        "images": [], 
        "object": "2000 Palace Rei, 18.000 m2, project", 
        "area": 18000
      }, 
      {
        "images": [
          "2001-aqua-park-btc-ljubljana.jpg"
        ], 
        "object": "2001 Aqua Park BTC, 14.000 m2, project", 
        "area": 14000
      }, 
      {
        "images": [], 
        "object": "2001 Offices Luize Pesjakove, 1.100 m2, constructed 2001", 
        "area": 1100
      }, 
      {
        "images": [], 
        "object": "2001 Shopping center Spar, 1.200 m2, constructed 2001", 
        "area": 1200
      }, 
      {
        "images": [
          "2003-aqua-park-btc-ljubljana.jpg"
        ], 
        "object": "2003 Aqua park BTC, 14.800 m2, constructed 2006", 
        "area": 14800
      }, 
      {
        "images": [], 
        "object": "2003 Business Complex Masarykova, 32.000 m2, project", 
        "area": 32000
      }, 
      {
        "images": [], 
        "object": "2003 Kodak cener, 3.600 m2, constructed 2003", 
        "area": 3600
      }, 
      {
        "images": [], 
        "object": "2003 Apartments and offices Viska cerkev, 5.800 m2, constructed 2003", 
        "area": 5800
      }, 
      {
        "images": [
          "2004-apartment-complex-kitajski-zid-ljubljana.jpg"
        ], 
        "object": "2004 Apartment Complex Kitajski zid, 36.650 m2, under construction, 1st award at the internal competition", 
        "area": 36650
      }, 
      {
        "images": [], 
        "object": "2004 Apartments Brdo, 85.000 m2, 2st award at the competition", 
        "area": 85000
      }, 
      {
        "images": [], 
        "object": "2005 Office and apartments building Zelena jama, competition", 
        "area": 0
      }, 
      {
        "images": [], 
        "object": "2005 Car park at Exhibition and Convention Centre, 19.100 m2, project", 
        "area": 19100
      }, 
      {
        "images": [], 
        "object": "2005 Apartments and offices Hermana Potocnika, 5.100 m2, constructed", 
        "area": 5100
      }, 
      {
        "images": [], 
        "object": "2005 Apartments and offices Njegosev kvart, 19.000 m2, constructed 2009", 
        "area": 19000
      }, 
      {
        "images": [
          "2006-business-complex-astra-ljubljana.jpg"
        ], 
        "object": "2006 Business Complex \u00abAstra\u00bb, 13.600 m2, under construction, 1st award at the internal competition", 
        "area": 13600
      }, 
      {
        "images": [
          "2006-residential-complex-ruski-car-ljubljana.jpg"
        ], 
        "object": "2006 Residential Complex Ruski car, 13.600 m2, project", 
        "area": 13600
      }, 
      {
        "images": [
          "2006-shopping-centre-btc-development-study.jpg"
        ], 
        "object": "2006 Shopping Centre BTC, development study, 356.000 m2, project", 
        "area": 356000
      }, 
      {
        "images": [], 
        "object": "2006 Academy Complex, 30.000 m2, project", 
        "area": 30000
      }, 
      {
        "images": [
          "2007-exhibition-and-convention-centre-ljubljana.jpg"
        ], 
        "object": "2007 Exhibition and Convention Centre, 13.400 m2, constructed 2007", 
        "area": 13400
      }, 
      {
        "images": [
          "2007-business-centre-btc-ljubljana.jpg"
        ], 
        "object": "2007 Business Centre \u00abBTC\u00bb, 47.000 m2, project", 
        "area": 47000
      }, 
      {
        "images": [], 
        "object": "2008 Nursing Hospital, 22.000 m2, project", 
        "area": 22000
      }, 
      {
        "images": [], 
        "object": "2010 Centromerkur department store, 4.500 m2, constructed 2010", 
        "area": 4500
      }, 
      {
        "images": [], 
        "object": "2010 Mercator center Smartinska, 16.600 m2, constructed 2010", 
        "area": 16600
      }, 
      {
        "images": [], 
        "object": "2010 Apartments Block \u00ab\u010copova\u00bb, 6.000 m2, project", 
        "area": 6000
      }, 
      {
        "images": [
          "2013-hotel-opova-ljubljana.jpg"
        ], 
        "object": "2013 Hotel \u00ab\u010copova\u00bb, 13.000 m2, project", 
        "area": 13000
      }, 
      {
        "images": [
          "2013-national-gallery-ljubljana.jpg"
        ], 
        "object": "2013 National Gallery, 4.700 m2, under construction", 
        "area": 4700
      }, 
      {
        "images": [
          "2000-gallery-kresija-ljubljana.jpg"
        ], 
        "object": "2000 Gallery Kresija", 
        "area": 0
      }, 
      {
        "images": [
          "2004-business-complex-masarykova-ljubljana.jpg"
        ], 
        "object": "2004 Business Complex Masarykova", 
        "area": 0
      }, 
      {
        "images": [
          "2008-office-building-elcom-ljubljana.jpg"
        ], 
        "object": "2008 Office building \u00abElcom\u00bb", 
        "area": 0
      }, 
      {
        "images": [
          "2010-department-store-ljubljana.jpg"
        ], 
        "object": "2010 Department store", 
        "area": 0
      }, 
      {
        "images": [
          "2011-hotel-meksika-ljubljana.jpg"
        ], 
        "object": "2011 Hotel Meksika", 
        "area": 0
      }, 
      {
        "images": [
          "2012-house-ko-ljubljana.jpg"
        ], 
        "object": "2012 House ko", 
        "area": 0
      }, 
      {
        "images": [
          "2013-kindergarten-ljubljana.jpg"
        ], 
        "object": "2013 Kindergarten", 
        "area": 0
      }, 
      {
        "images": [
          "2013-emergency-department-ljubljana.jpg"
        ], 
        "object": "2013 Emergency department", 
        "area": 0
      }
    ]
  }, 
  "Krasnodar, Russia": {
    "position": {
      "lat": 45.03926740000001, 
      "lng": 38.987221
    }, 
    "objects": [
      {
        "images": [
          "2006-pediatric-clinic-krasnodar.jpg"
        ], 
        "object": "2006 Pediatric Clinic, 28.000 m2, project", 
        "area": 28000
      }
    ]
  }, 
  "Prague, Czech Republic": {
    "position": {
      "lat": 50.0755381, 
      "lng": 14.4378005
    }, 
    "objects": [
      {
        "images": [], 
        "object": "1990 Hotel Amadeus, 7.000 m2, project", 
        "area": 7000
      }, 
      {
        "images": [], 
        "object": "2004 Aqua Park Shutka, 25.900 m2, project", 
        "area": 25900
      }, 
      {
        "images": [
          "2007-hotel-na-petynce-prague.jpg"
        ], 
        "object": "2007 Hotel \u00abNa Petynce\u00bb, 35.700 m2, project", 
        "area": 35700
      }
    ]
  }, 
  "Istanbul, Turkey": {
    "position": {
      "lat": 41.0082376, 
      "lng": 28.9783589
    }, 
    "objects": [
      {
        "images": [
          "1990-business-centre-nasco-istanbul.jpg"
        ], 
        "object": "1990 Business Centre Nasco, 15.000 m2, project", 
        "area": 15000
      }
    ]
  }, 
  "Podgorica, Montenegro": {
    "position": {
      "lat": 42.4304196, 
      "lng": 19.2593642
    }, 
    "objects": [
      {
        "images": [], 
        "object": "2002 Market place, 90.000 m2, project", 
        "area": 90000
      }
    ]
  }
}