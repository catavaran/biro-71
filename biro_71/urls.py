from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin

from arch_objects import views

urlpatterns = [
    url(r'^$', views.objects_map),
    url(r'^objects-list/$', views.objects_list, name='objects-list'),
    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
