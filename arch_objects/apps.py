from __future__ import unicode_literals

from django.apps import AppConfig


class ArchObjectsConfig(AppConfig):
    name = 'arch_objects'
    verbose_name = 'Architectural objects'
