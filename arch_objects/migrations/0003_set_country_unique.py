# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-22 11:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('arch_objects', '0002_create_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='name_en',
            field=models.CharField(max_length=50, unique=True, verbose_name='name (en)'),
        ),
    ]
