from django import forms
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from .models import *


class CountryAdmin(admin.ModelAdmin):
    list_display = ['name_en', 'name_ru']

admin.site.register(Country, CountryAdmin)


class ArchObjectInline(admin.TabularInline):
    model = ArchObject
    fields = ['name_en', 'name_ru', 'display_images', 'display_edit_link']
    readonly_fields = ['display_images', 'display_edit_link']
    extra = 0

    def display_images(self, obj):
        return u' '.join(
            u'<a href="{0}"><img src="{0}" height="20"><a>'.format(img.image.url)
            for img in obj.images.all())
    display_images.short_description = _('Images')
    display_images.allow_tags = True

    def display_edit_link(self, obj):
        return u'<a href="{0}">{1}</a>'.format(
            reverse('admin:arch_objects_archobject_change', args=[obj.id]),
            _('Edit'))
    display_edit_link.short_description = _('Edit')
    display_edit_link.allow_tags = True


class CityForm(forms.ModelForm):

    def clean_name_en(self):
        name_en = self.cleaned_data['name_en']
        self.country = Country.objects.get_country_from_string(name_en)
        if not self.country:
            raise forms.ValidationError(_('Unknown country.'))
        return name_en

    def save(self, *args, **kwargs):
        self.instance.country = self.country
        if not (self.instance.lat and self.instance.lng):
            self.instance.retrieve_geo_location()
        return super(CityForm, self).save(*args, **kwargs)


class CityAdmin(admin.ModelAdmin):
    list_display = ['name_en', 'name_ru']
    list_display_links = ['name_en', 'name_ru']
    search_fields = ['name_en', 'name_ru']
    form = CityForm
    fields = ['name_en', 'name_ru', 'lat', 'lng']
    inlines = [ArchObjectInline]

    class Media:
        css = {'all': ('css/admin/hide_original.css',
                       'css/admin/arch_object.css')}

admin.site.register(City, CityAdmin)


class ImageInline(admin.TabularInline):
    model = Image
    extra = 0


class ArchObjectAdmin(admin.ModelAdmin):
    list_display = ['name_en', 'city']
    fields = ['city', 'name_en', 'name_ru']
    search_fields = ['name_en', 'name_ru', 'city__name_en', 'city__name_ru']
    inlines = [ImageInline]

    class Media:
        css = {'all': ('css/admin/hide_original.css', )}

admin.site.register(ArchObject, ArchObjectAdmin)
