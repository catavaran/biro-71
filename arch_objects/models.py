from __future__ import unicode_literals

import re
import requests

from django.db import models
from django.utils.translation import ugettext_lazy as _

AREA_RE = re.compile(r'([\d\s\.]+)\s*m2')


class CountryManager(models.Manager):

    def get_country_from_string(self, s):
        """
        Find country name in the comma-separated string
        """
        bits = [b.strip() for b in s.split(',')]
        for country in self.all():
            if country.name_en in bits:
                return country
        return None


class Country(models.Model):

    name_en = models.CharField(_('name (en)'), max_length=50, unique=True)
    name_ru = models.CharField(_('name (ru)'), max_length=50, null=True, blank=True)

    objects = CountryManager()

    class Meta:
        verbose_name = _('country')
        verbose_name_plural = _('countries')

    def __unicode__(self):
        return self.name_en



class City(models.Model):

    country = models.ForeignKey(Country, related_name='cities',
                                verbose_name=_('country'))

    name_en = models.CharField(_('name (en)'), max_length=50)
    name_ru = models.CharField(_('name (ru)'), max_length=50, null=True)

    lat = models.DecimalField(_('latitude'), max_digits=10, decimal_places=6,
                              null=True, blank=True)
    lng = models.DecimalField(_('longitude'), max_digits=10, decimal_places=6,
                              null=True, blank=True,
                              help_text=_('Leave blank to populate these fields'
                                          ' from Google Maps API'))

    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')
        ordering = ['name_en']

    def __unicode__(self):
        return self.name_en

    def retrieve_geo_location(self):
        """
        Obtain coordinates using Google Map API and set lat/lng fields.
        Set these fields to None in case of failed geolocation.
        """
        r = requests.get('http://maps.googleapis.com/maps/api/geocode/json',
                         params={'address': self.name_en,
                                 'sensor': 'false'})
        try:
            location = r.json()['results'][0]['geometry']['location']
            self.lat, self.lng = location['lat'], location['lng']
        except IndexError:
            self.lat, self.lng = None, None


class ArchObject(models.Model):

    city = models.ForeignKey(City, related_name='arch_objects',
                             verbose_name=_('city'))

    name_en = models.CharField(_('name (en)'), max_length=250)
    name_ru = models.CharField(_('name (ru)'), max_length=250, null=True)

    area = models.IntegerField(_('area (m2)'), null=True)

    class Meta:
        verbose_name = _('architectural object')
        verbose_name_plural = _('architectural objects')
        ordering = ['name_en']

    def __unicode__(self):
        return self.name_en

    def save(self, *args,**kwargs):
        m = AREA_RE.search(self.name_en)
        if m:
            self.area = int(m.group(1).replace(' ', '').replace('.', ''))
        else:
            self.area = None
        super(ArchObject, self).save(*args, **kwargs)



class Image(models.Model):

    arch_object = models.ForeignKey(ArchObject, related_name='images',
                                    verbose_name=_('architectural objects'))
    image = models.ImageField(upload_to='photo', max_length=250)

    class Meta:
        verbose_name = _('image')
        verbose_name_plural = _('images')
