# coding: utf-8

import collections
import os
import re
import requests
import time
import unicodedata

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from arch_objects.models import *


NON_FILENAME_CHARS = re.compile('[^\w, \.-]+')
COMMA_SPACE_RE = re.compile('[, \t]+')


class Command(BaseCommand):

    help = 'Import architectural objects data from objects.txt'

    def handle(self, *args, **options):

        self.stdout.write(self.style.WARNING(
                        'All current records in the database will be deleted!'))
        answer = raw_input('Are you sure? [y/n] ')
        if answer != 'y':
            return

        locations = self.load_locations()

        answer = raw_input(
            'Ready to get geocode data and re-populate the database. '
            'Proceed? [y/n] ')

        if answer == 'y':

            self.populate_geo_position(locations)

            with transaction.atomic():

                Image.objects.all().delete()
                ArchObject.objects.all().delete()
                City.objects.all().delete()

                for location, info in locations.iteritems():
                    self.create_city_objects(location, info)

            self.stdout.write(self.style.SUCCESS('Done.'))


    def create_city_objects(self, location, info):
        """
        Create database records for single city
        """
        country = Country.objects.get(name_en=info['country'])
        position = info.get('position') or {'lat': None, 'lng': None}
        city = City.objects.create(country=country,
                                   name_en=location, name_ru=location,
                                   lat=position['lat'],
                                   lng=position['lng'])
        for obj in info['objects']:
            arch_obj = ArchObject.objects.create(
                                city=city,
                                name_en=obj['object'], name_ru=obj['object'])
            for image in obj['images']:
                Image.objects.create(arch_object=arch_obj,
                                     image=u'photo/%s' % image)



    def get_location(self, line):
        """
        Return object's location and description line w/o this location
        """
        bits = [b.strip() for b in line.split(',')]

        country = Country.objects.get_country_from_string(line)
        if not country:
            self.stdout.write(self.style.ERROR(u"Unknown country: [%s]" % line))
            return None, None, ', '.join(bits)

        country_name = country.name_en
        i = bits.index(country_name)
        if i == 1:
            self.stdout.write(self.style.WARNING(u"No city: [%s]" % line))
            location = country_name
            del bits[i] # delete country
        else:
            location = ', '.join(bits[i-1:i+1])
            del bits[i-1] # delete city
            del bits[i-1] # delete country
        return country_name, location, ', '.join(bits)


    def normalize_str(self, s):
        """
        Lowercase string, remove "non-filename" symbols and replace commas/spaces
        with hyphens
        """
        s = unicodedata.normalize('NFC', s)
        s = NON_FILENAME_CHARS.sub('', s)
        return COMMA_SPACE_RE.sub('-', s.lower())


    def get_images(self, images, line):
        """
        Return list of images associated with the object
        """
        object_images = []
        normalized_line = self.normalize_str(line)
        for key in list(images.keys()):
            if normalized_line.startswith(key.rstrip('0123456789-')):
                object_images.append(images[key])
                del images[key]
        return object_images


    def load_images(self):
        """
        Load list of photos
        """
        path_to_images = os.path.join(settings.MEDIA_ROOT, 'photo')
        images = collections.OrderedDict()
        for file_name in sorted(os.listdir(path_to_images)):
            file_name = unicode(file_name, 'utf-8')
            bits = self.normalize_str(file_name).split('.')
            if len(bits) > 1 and bits[-1] in ('jpg', 'jpeg', 'png'):
                images[bits[0]] = file_name
        return images


    def load_locations(self):
        """
        Load objects descriptions and group them by location
        """
        images = self.load_images()
        locations = {}
        for line in open('objects.txt').readlines():
            line = unicode(line, 'utf-8')
            line = line.rstrip('\n').rstrip('\r')
            if line.startswith('----'):
                continue
            country, location, description = self.get_location(line)
            if location:
                data = locations.setdefault(location, {'objects': [],
                                                       'country': country})
                data['objects'].append({'object': description,
                                        'images': self.get_images(images, line)})
        if images:
            self.stdout.write(self.style.WARNING(
                              u"Unhandled images found: %s" % images.values()))
        return locations


    def populate_geo_position(self, locations):
        """
        Add geocoding information to each location
        """
        cities_by_geo = {}
        for location, obj in locations.iteritems():
            r = requests.get(
                'http://maps.googleapis.com/maps/api/geocode/json',
                params={'address': location, 'sensor': 'false'})
            try:
                obj['position'] = r.json()['results'][0]['geometry']['location']
                self.stdout.write("%s -> %s" % (location, obj['position']))
                geo_key = '%.04f-%.04f' % (obj['position']['lat'], obj['position']['lng'])
                cities = cities_by_geo.setdefault(geo_key, [])
                cities.append(location)
            except IndexError:
                self.stdout.write(self.style.ERROR("Can not get location for %s" % location))
                obj['position'] = None
            time.sleep(1)

        for _, cities in cities_by_geo.iteritems():
            if len(cities) > 1:
                self.stdout.write(self.style.WARNING("Possible same locations: %s" % cities))

