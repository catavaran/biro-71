# coding: utf-8

import collections
import re

from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import F

from arch_objects.models import *

AREA_RE = re.compile(ur'([\d\s\.]+)\s*м2')
MULTI_COMMA_RE = re.compile(r',[,\s]+')



class Command(BaseCommand):

    help = 'Update russian translation using the objects-ru.txt'

    def handle(self, *args, **options):

        self.cities = collections.OrderedDict()

        with transaction.atomic():
            #ArchObject.objects.all().update(name_ru=F('name_en'))

            current_pk = 0
            for line in open('objects-ru.txt').readlines():

                line = unicode(line, 'utf-8').strip()
                if not line:
                    continue

                if '|' in line:
                    self.add_city_translation(line)
                    continue

                self.translate_object(line)


        self.stdout.write(self.style.SUCCESS('Done.'))

    def add_city_translation(self, line):
        bits = line.split('|')
        self.cities[bits[0].strip()] = bits[1].strip()

    def get_city(self, line):
        for city_ru, city_en in self.cities.iteritems():
            if city_ru in line:
                city = City.objects.filter(name_en=city_en).first()
                if city:
                    if city.name_ru != city_ru:
                        city.name_ru = city_ru
                        city.save()
                    return city, MULTI_COMMA_RE.sub(', ', line.replace(city_ru, ''))
                else:
                    self.stdout.write(self.style.ERROR(u'Invalid translation for [%s]' % city_ru))
        self.stdout.write(self.style.ERROR(u'No city found in [%s]' % line))        
        return None, line


    def translate_object(self, line):
        city, line = self.get_city(line)

        if not city:
            return

        m = AREA_RE.search(line)
        if not m:
            self.stdout.write(self.style.WARNING(u'No area: %s -> %s' % (city, line)))
            return

        area = int(m.group(1).replace(' ', '').replace('.', ''))

        arch_objects = list(ArchObject.objects.filter(city=city, area=area))
        if len(arch_objects) == 0:
            self.stdout.write(self.style.ERROR(u'No object with such area: %s -> %s' % (city, line)))
        elif len(arch_objects) > 1:
            self.stdout.write(self.style.WARNING(u'More than one object with such area: %s -> %s' % (city, line)))
            for o in arch_objects:
                self.stdout.write(self.style.WARNING(u' - %s' % o.name_en))
        else:
            arch_objects[0].name_ru = arch_objects[0].name_en[:5] + line
            arch_objects[0].save()
