import json
import re

from django.shortcuts import render

from .models import *


def objects_map(request):

    # Naive implementation like
    #
    #     for city in cities:
    #         for obj in city.arch_objects:
    #             for image in obj.images:
    #
    # will produce hundreds of queries even with `prefetch_related()` so
    # lets prefetch all data in three SQL calls by ourselves.

    is_ru = request.GET.get('lang', '') == 'ru'

    images = {} # images by arch_object.id
    for img in Image.objects.all():
        img_list = images.setdefault(img.arch_object_id, [])
        img_list.append(img.image.url)

    trans_state = {} # are all city objects transalted?
    arch_objects = {} # arch objects by city.id
    for obj in ArchObject.objects.all():
        trans_state.setdefault(obj.city_id, True)
        if is_ru and obj.name_ru == obj.name_en:
            trans_state[obj.city_id] = False
        obj_list = arch_objects.setdefault(obj.city_id, [])
        obj_list.append({'object': obj.name_ru if is_ru else obj.name_en,
                         'area': obj.area,
                         'project': 'construct' not in obj.name_en,
                         'images': images.get(obj.id, [])})

    cities = {}
    for city in City.objects.all():
        if city.lat and city.lng:
            city_name = city.name_ru if is_ru else city.name_en
            cities[city_name] = {'position': {'lat': float(city.lat),
                                              'lng': float(city.lng)},
                                 'trans_state': trans_state[city.id],
                                 'objects': arch_objects.get(city.id, [])}

    return render(request, 'objects-map.html', {'cities': json.dumps(cities)})


def objects_list(request):

    name_attr = 'name_ru' if request.GET.get('lang', '') == 'ru' else 'name_en'
    comma_area_re = re.compile(',\s+(\d)')

    def object_name(obj):
        name = getattr(obj, name_attr)
        city_name = getattr(obj.city, name_attr)
        if comma_area_re.search(name):
            name = comma_area_re.sub(u', ' + city_name + r', \1', name)
        else:
            name += ', ' + city_name
        return name

    countries = []
    for country in Country.objects.all().order_by(name_attr):
        objects = [object_name(obj) for obj in 
                    ArchObject.objects.select_related('city')
                                      .filter(city__country=country)
                                      .order_by(name_attr)]
        countries.append({'name': getattr(country, name_attr),
                          'objects': objects})

    return render(request, 'objects-list.html', {'countries': countries})
