# BIRO-71 #

Project to place a list of architectural projects on a google map.

### Setup ###

Create virtualenv, activate it and then run the following commands:

    pip install -r requirements.txt
    python manage.py migrate
    python manage.py createsuperuser
    python manage.py load initial_countries

### Workflow ###

1. Edit the list of projects/constructions in the `objects.txt`

2. Put images of objects into the `media/photo` directory. Image should have
   the same name as the corresponding object with an optional number.
   For example: `1968 Cultural Centre 1.png`. For convenience' sake image
   name can be safely "slugified" like this: `1968-cultural-centre-1.png`.

3. Run `python manage.py importobjects` to import object list into
   the database.  Warning: this script deletes all previously loaded
   data.

4. Run `python manage.py translateru` to add russian translations.

5. Start web server by executing `python manage.py runserver` and point
   your browser to http://127.0.0.1:8000

6. Enjoy.
