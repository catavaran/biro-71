var map;
var markers = [];
var markerClusterer = null;
var lastInfoWindow = null;

var galleryDetailId = 0;

var markerImage = {
	url: '/static/img/pin-48x48.png'
};

var clusterStyles = [
	{
		textColor: 'black',
		anchorText: [-11, 0],
		url: '/static/img/pin-56x56.png',
		height: 56,
		width: 56
    },
	{
		textColor: 'black',
		anchorText: [-11, 0],
		url: '/static/img/pin-56x56.png',
		height: 56,
		width: 56
    },
	{
		textColor: 'black',
		anchorText: [-13, 0],
		url: '/static/img/pin-64x64.png',
		height: 64,
		width: 64
    }
];


function getGalleryDetailClass() {
	galleryDetailId += 1;
	return 'gdtl-' + galleryDetailId;
}


function addMarker(properties) {
    var marker = new MarkerWithLabel(properties);
    markers.push(marker);
    return marker;
}


function setMapOnAllMarkers(map) {
    $.each(markers, function (i, marker) {
        marker.setMap(map);
    });
}


function sumObjects(markers) {
    var sum = 0;
    $.each(markers, function (i, marker) {
        sum += marker.value;
    });
    var index = parseInt(sum / 30, 10) + 1;
    return {
        index: index,
        text: sum
    }
}


function showMarkers() {

    var showProjects = $('#cb-projects').is(":checked");
    var showConstructions = $('#cb-constructions').is(":checked");
    var withPhoto = $('#cb-photo').is(":checked");
    var useClusters = $('#cb-cluster').is(":checked");

    if (lastInfoWindow) {
        lastInfoWindow.close();
        lastInfoWindow = null;
    }
    if (markerClusterer) {
        markerClusterer.clearMarkers();
    }
    setMapOnAllMarkers(null);
    markers = [];

    var totalArea = 0;

    $.each(cities, function (city, info) {
        var infoText = '';
		var infoCount = 0;
        $.each(info.objects, function (i, obj) {
            var show = (showProjects && showConstructions) ||
					   (showProjects && obj.project) ||
                       (showConstructions && !obj.project);
            if (withPhoto && obj.images.length == 0) {
                show = false;
            }
            if (show) {
                infoText += '<p class="object-descr">';
				if (obj.images.length > 0) {
					$.each(obj.images, function (j, img) {
						var galClass = getGalleryDetailClass();
						infoText += '<a href=".' + galClass + '">' +
										'<span class="' + galClass +'">' +
											'<span class="detail">' + obj.object + '</span>' +
											'<img src="' + img + '">' +
										'</span>' +
										(j == 0 ? obj.object : '') +
									'</a>';
					});
				} else {
					var galClass = getGalleryDetailClass();
					infoText += '<a href=".' + galClass + '">' +
									obj.object +
									'<span class="' + galClass +'">' +
										'<span class="detail"><span class="nophoto">' + obj.object + '</span></span>' +
									'</span>' +
								'</a>';
				}
                infoText += '</p><div style="clear: both"></div>';
                totalArea += obj.area;
				infoCount += 1;
            }
        });
        if (infoText && info.position) {
            var infoWindow = new google.maps.InfoWindow({
                maxWidth: 400,
                content: '<b>' + city + '</b>' + infoText
            });
            var marker = addMarker({
				icon: markerImage,
                position: info.position,
                title: city,
				labelContent: '' + infoCount,
				labelAnchor: new google.maps.Point(20, 42),
				labelClass: 'labels' + (info.trans_state ? '' : ' labels-missing-trans'),
				labelStyle: {opacity: 1}
            });
            marker.value = infoCount;
            marker.addListener('click', function() {
                if (lastInfoWindow) {
                    lastInfoWindow.close();
                }
                infoWindow.open(map, marker);
                lastInfoWindow = infoWindow;

				$('p.object-descr a').featherlightGallery({
					previousIcon: '&lsaquo;',
					nextIcon: '&rsaquo;',
					galleryFadeIn: 300,
					openSpeed: 300
				});

            });
        }
    });

    if (totalArea) {
        var areaText = totalArea.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        $('#txt-area').html('Площадь: ' + areaText + ' м2');
    } else {
        $('#txt-area').html('');
    }

    if (useClusters) {
        markerClusterer = new MarkerClusterer(map, markers, {
			styles: clusterStyles,
            calculator: sumObjects,
            gridSize: 30
        });
    } else {
        setMapOnAllMarkers(map);
    }

}

$(function () {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 40.397, lng: 30.644},
        zoom: 2
    });

    $('.cb-control').change(showMarkers);

    showMarkers();


});

