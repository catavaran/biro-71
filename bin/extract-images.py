#!/usr/bin/env python

import base64 
import lxml.html
import re
import unicodedata


NON_FILENAME_CHARS = re.compile('[^\w, -]+')
COMMA_SPACE_RE = re.compile('[, \t-]+')

def make_file_name(s):
    """
    Makes the normalized filename from object's description
    """
    s = u'-'.join(s.split(',')[:2])
    s = s[0:4] + '-' + s[4:]
    s = unicodedata.normalize('NFC', s)
    s = NON_FILENAME_CHARS.sub('-', s)

    return COMMA_SPACE_RE.sub('-', s.lower())


html = lxml.html.parse("catalogue.html")

unknown_cnt = 1

for el in html.xpath("//span[contains(@class,'fr1') or contains(@class ,'fr2btag')]"):

    parent = el.getparent() # surrounding div
    p = parent.getnext().getnext() # skip the next empty div and get the P tag
    if p.tag == 'p':
        file_name = make_file_name(p.text_content())
    else:
        file_name = 'unknown-%03d' % unknown_cnt
        unknown_cnt += 1

    img = el.find('img')
    img_data = base64.b64decode(img.attrib['src'].split(',')[1])

    with open('photo/%s.jpg' % file_name, 'w') as f:
        f.write(img_data)

    print file_name
