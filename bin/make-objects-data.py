#!/usr/bin/env python
# coding: utf-8

import collections
import json
import os
import re
import unicodedata
import urllib
import urllib2


COUNTRIES = ['Russia', 'Serbia', 'Germany', 'Ukraine', 'Turkey',
             'Egypt', 'Slovenia', 'Kazakhstan', 'Tunisia', 'Iraq',
             'Finland', 'Italy', 'Poland', 'Montenegro', 'Cuba',
             'Croatia', 'Armenia', 'Tanzania', 'Macedonia',
             'Czech Republic', 'Bosnia and Herzegovina',
             'Algeria', 'Nepal', 'Maldives', 'Mongolia',
             'Jordan', 'São Tome e Principe', 'Djibouti', 'Cyprus',
             'Latvia', 'Albania', 'Romania', 'New Caledonia', 'Lebanon',
             'Switzerland', 'USA', 'Uzbekistan', 'Hungary', 'Georgia',
             'Turkmenistan', 'Zimbabwe', 'Tadjikistan',
             'United Arab Emirates', 'Kosovo', 'Iran', 'Iraq']


def get_location(line):
    """
    Return object's location and description line w/o this location
    """
    bits = [b.strip() for b in line.split(',')]
    for c in COUNTRIES:
        if c in bits:
            i = bits.index(c)
            if i == 1:
                print("----WARNING: No city: [%s]" % line)
                location = c
                del bits[i] # delete country
            else:
                location = ', '.join(bits[i-1:i+1])
                del bits[i-1] # delete city
                del bits[i-1] # delete country
            return location, ', '.join(bits)
    print("----WARNING: Unknown country: [%s]" % line)
    return None, ', '.join(bits)


NON_FILENAME_CHARS = re.compile('[^\w, \.-]+')
COMMA_SPACE_RE = re.compile('[, \t]+')

def normalize_str(s):
    """
    Lowercase string, remove "non-filename" symbols and replace commas/spaces
    with hyphens
    """
    s = unicodedata.normalize('NFC', unicode(s, 'utf-8'))
    s = NON_FILENAME_CHARS.sub('', s)
    return COMMA_SPACE_RE.sub('-', s.lower())


def get_images(images, line):
    """
    Return list of images associated with the object
    """
    object_images = []
    normalized_line = normalize_str(line)
    for key in list(images.keys()):
        if normalized_line.startswith(key.rstrip('0123456789-')):
            object_images.append(images[key])
            del images[key]
    return object_images


AREA_RE = re.compile(r'(\d+\s*\.\d+)\s*m2')

def get_area(line):
    """
    Extract object's area
    """
    m = AREA_RE.search(line)
    if m:
        return int(m.group(1).replace(' ', '').replace('.', ''))
    return 0


def load_images():
    """
    Load list of photos
    """
    images = collections.OrderedDict()
    for file_name in sorted(os.listdir('photo')):
        bits = normalize_str(file_name).split('.')
        if len(bits) > 1 and bits[-1] in ('jpg', 'jpeg', 'png'):
            images[bits[0]] = file_name
    return images


def load_locations():
    """
    Load objects descriptions and group them by location
    """
    images = load_images()
    locations = {}
    for line in open('objects.txt').readlines():
        line = line.rstrip('\n').rstrip('\r')
        if line.startswith('----'):
            continue
        location, description = get_location(line)
        if location:
            data = locations.setdefault(location, {'objects': []})
            data['objects'].append({'object': description,
                                    'area': get_area(line),
                                    'images': get_images(images, line)})
    if images:
        print("----WARNING: Unhandled images found: %s" % images.values())
    return locations


def populate_geo_position(locations):
    """
    Add geocoding information to each location
    """
    cities_by_geo = {}
    for location, obj in locations.iteritems():
        response = urllib2.urlopen(
            'http://maps.googleapis.com/maps/api/geocode/json?%s' %
            urllib.urlencode({'address': location, 'sensor': 'false'}))
        data = json.load(response)
        try:
            obj['position'] = data['results'][0]['geometry']['location']
            print("%s -> %s" % (location, obj['position']))
            geo_key = '%.04f-%.04f' % (obj['position']['lat'], obj['position']['lng'])
            cities = cities_by_geo.setdefault(geo_key, [])
            cities.append(location)
        except IndexError:
            print("----ERROR: Can not get location for %s" % location)
            obj['position'] = None

    for _, cities in cities_by_geo.iteritems():
        if len(cities) > 1:
            print("----WARNING: Possible same locations: %s" % cities)

# main ------------------------------------------------------------------------

locations = load_locations()

answer = raw_input(
    'Ready to get geocode data and to overwrite the objects-data.js file. '
    'Proceed? [y/n] ')

if answer == 'y':

    populate_geo_position(locations)

    with open('objects-data.js', 'w') as f:
        f.write('var cities = ')
        f.write(json.dumps(locations, indent=2))

    print('Done.')
